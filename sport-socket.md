## Recieve Common format

```json
{
    "type": "event_type",
    "data": {//format base on event_type
        ...
    }
}
```

## live match

### connection path

- /live

### emit obj

```json
{
  "subscribeByMatch":true,
  "matchIdSet": [2897, 2898, 2899, 1], //if is null or empty array will recive all match info
  "language": "en" // default en
}
```

### recieve objs

#### matchInfo

- type: "matchInfo"
- data:

```json
{
  "id": 3484,
  "sportId": "basketball",
  "leagueId": 458,
  "seasonId": 18,
  "round": 1,
  "matchStatus": {
    "id": "ended",
    "status": "end",
    "name": "ended"
  },
  "startAt": "2021-02-05T03:00:00",
  "venue": {
    "id": 116,
    "name": "Staples Center",
    "capacity": 18997,
    "mapCoordinates": "34.043059,-118.267223",
    "country": {
      "id": "USA",
      "name": "United States of America"
    }
  },
  "played": "48:00",
  "remaining": "00:00",
  "matchTeams": [
    {
      "id": 6962,
      "matchId": 3484,
      "teamType": "home",
      "team": {
        "id": 118,
        "name": "Los Angeles Lakers",
        "abbreviation": "LAL",
        "logo": "https://fp-sports-data.oss-cn-shanghai.aliyuncs.com/logo/team/NBA/LosAngelesLakers.png",
        "venue": {
          "id": 116,
          "name": "Staples Center",
          "capacity": 18997,
          "mapCoordinates": "34.043059,-118.267223",
          "country": {
            "id": "USA",
            "name": "United States of America"
          }
        },
        "gender": "male",
        "country": {
          "id": "USA",
          "name": "United States of America"
        }
      }
    }
  ],
  "matchScores": [
    {
      "id": 25677,
      "matchId": 3484,
      "teamType": "home",
      "periodType": "regular_period",
      "order": 3,
      "score": 37
    }
  ]
}
```

#### liveMatchList

- type: "liveMatchList"
- data:

```json
[
  {
    "id": 3484,
    "sportId": "basketball",
    "leagueId": 458,
    "seasonId": 18,
    "round": 1,
    "matchStatus": {
      "id": "ended",
      "status": "end",
      "name": "ended"
    },
    "startAt": "2021-02-05T03:00:00",
    "venue": {
      "id": 116,
      "name": "Staples Center",
      "capacity": 18997,
      "mapCoordinates": "34.043059,-118.267223",
      "country": {
        "id": "USA",
        "name": "United States of America"
      }
    },
    "played": "48:00",
    "remaining": "00:00",
    "matchTeams": [
      {
        "id": 6962,
        "matchId": 3484,
        "teamType": "home",
        "team": {
          "id": 118,
          "name": "Los Angeles Lakers",
          "abbreviation": "LAL",
          "logo": "https://fp-sports-data.oss-cn-shanghai.aliyuncs.com/logo/team/NBA/LosAngelesLakers.png",
          "venue": {
            "id": 116,
            "name": "Staples Center",
            "capacity": 18997,
            "mapCoordinates": "34.043059,-118.267223",
            "country": {
              "id": "USA",
              "name": "United States of America"
            }
          },
          "gender": "male",
          "country": {
            "id": "USA",
            "name": "United States of America"
          }
        }
      }
    ],
    "matchScores": [
      {
        "id": 25677,
        "matchId": 3484,
        "teamType": "home",
        "periodType": "regular_period",
        "order": 3,
        "score": 37
      }
    ]
  }
]
```

#### matchEvent

- type: "matchEvent"
- data:

```json
{
  "id": 1042541,
  "matchId": 221,
  "eventType": {
    "id": "match_ended",
    "name": "match_ended"
  },
  "order": 146,
  "matchTime": null,
  "matchClock": "90:00",
  "teamType": null,
  "x": null,
  "y": null,
  "period": null,
  "periodType": null,
  "outcome": null,
  "breakName": null,
  "matchEventPlayers": null
}
```
