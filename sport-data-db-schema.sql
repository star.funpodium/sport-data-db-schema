CREATE TABLE `sport` (
  `id` varchar(64) PRIMARY KEY,
  `create_at` timestamp DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);

CREATE TABLE `league` (
  `id` bigint(20) PRIMARY KEY AUTO_INCREMENT,
  `sport_id` varchar(64),
  `name` varchar(64) DEFAULT null,
  `logo` varchar(255) DEFAULT null,
  `confederation` varchar(64) DEFAULT null,
  `country_code` varchar(64) DEFAULT null,
  `create_at` timestamp DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);
CREATE TABLE `season` (
  `id` bigint(20) PRIMARY KEY AUTO_INCREMENT,
  `league_id` bigint(20),
  `name` varchar(64) DEFAULT null,
  `start_at` bigint(20) NOT NULL,
  `end_at` bigint(20) NOT NULL,
  `year` varchar(64) NOT NULL,
  `create_at` timestamp DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);

CREATE TABLE `venue` (
  `id` bigint(20) PRIMARY KEY AUTO_INCREMENT,
  `name` varchar(64) DEFAULT null,
  `capacity` bigint(20) DEFAULT null,
  `map_coordinates` varchar(64) DEFAULT null,
  `country_code` varchar(64) DEFAULT null,
  `create_at` timestamp DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);

CREATE TABLE `team` (
  `id` bigint(20) PRIMARY KEY AUTO_INCREMENT,
  `name` varchar(64) DEFAULT null,
  `country_code` varchar(64) DEFAULT null,
  `abbreviation` varchar(64) DEFAULT null,
  `logo` varchar(255) DEFAULT null,
  `venue_id` bigint(20),
  `gender` varchar(64) DEFAULT null,
  `create_at` timestamp DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);

CREATE TABLE `match` (
  `id` bigint(20) PRIMARY KEY AUTO_INCREMENT,
  `sport_id` varchar(64),
  `league_id` bigint(20),
  `season_id` bigint(20),
  `round` bigint(20) DEFAULT null,
  `match_status_id` varchar(64),
  `start_at` bigint(20) NOT NULL,
  `played` varchar(64) DEFAULT null,
  `remaining` varchar(64) DEFAULT null,
  `venue_id` bigint(20),
  `create_at` timestamp DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);

CREATE TABLE `match_status` (
  `id` varchar(64) PRIMARY KEY,
  `status` ENUM ('not_started', 'live', 'end', 'cancel','postponed'),
  `create_at` timestamp DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);

CREATE TABLE `player` (
  `id` bigint(20) PRIMARY KEY AUTO_INCREMENT,
  `name` varchar(64) DEFAULT null,
  `logo` varchar(255) DEFAULT null,
  `position_id` varchar(64),
  `date_of_birth` varchar(64) DEFAULT null,
  `nationality` varchar(64) DEFAULT null,
  `country_code` varchar(64) DEFAULT null,
  `height` varchar(64) DEFAULT null,
  `weight` varchar(64) DEFAULT null,
  `jersey_number` varchar(64) DEFAULT null,
  `preferred_foot` varchar(64) DEFAULT null,
  `place_of_birth` varchar(64) DEFAULT null,
  `gender` varchar(64) DEFAULT null,
  `create_at` timestamp DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);

CREATE TABLE `match_score` (
  `id` bigint(20) PRIMARY KEY AUTO_INCREMENT,
  `match_id` bigint(20),
  `team_type` ENUM ('home', 'away'),
  `period_type` ENUM ('regular_period', 'overtime', 'entire'),
  `order` bigint(20) DEFAULT 0,
  `score` int(10) DEFAULT 0,
  `create_at` timestamp DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);

CREATE TABLE `match_team` (
  `id` bigint(20) PRIMARY KEY AUTO_INCREMENT,
  `match_id` bigint(20),
  `team_type` ENUM ('home', 'away'),
  `team_id` bigint(20),
  `create_at` timestamp DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);

CREATE TABLE `match_team_stats` (
  `id` bigint(20) PRIMARY KEY AUTO_INCREMENT,
  `match_team_id` bigint(20),
  `assists` varchar(64) DEFAULT null,
  `rebounds` varchar(64) DEFAULT null,
  `three_point_attempts_successful` varchar(64) DEFAULT null,
  `three_point_attempts_total` varchar(64) DEFAULT null,
  `two_point_attempts_successful` varchar(64) DEFAULT null,
  `two_point_attempts_total` varchar(64) DEFAULT null,
  `free_throw_attempts_successful` varchar(64) DEFAULT null,
  `free_throw_attempts_total` varchar(64) DEFAULT null,
  `offensive_rebounds` varchar(64) DEFAULT null,
  `defensive_rebounds` varchar(64) DEFAULT null,
  `turnovers` varchar(64) DEFAULT null,
  `steals` varchar(64) DEFAULT null,
  `shots_blocked` varchar(64) DEFAULT null,
  `fouls` varchar(64) DEFAULT null,
  `team_rebounds` varchar(64) DEFAULT null,
  `team_turnovers` varchar(64) DEFAULT null,
  `timeouts` varchar(64) DEFAULT null,
  `yellow_cards` varchar(64) DEFAULT null,
  `yellow_red_cards` varchar(64) DEFAULT null,
  `red_cards` varchar(64) DEFAULT null,
  `corner_kicks` varchar(64) DEFAULT null,
  `substitutions` varchar(64) DEFAULT null,
  `ball_possession` varchar(64) DEFAULT null,
  `free_kicks` varchar(64) DEFAULT null,
  `goal_kicks` varchar(64) DEFAULT null,
  `throw_ins` varchar(64) DEFAULT null,
  `shots_on_target` varchar(64) DEFAULT null,
  `shots_total` varchar(64) DEFAULT null,
  `shots_off_target` varchar(64) DEFAULT null,
  `shots_saved` varchar(64) DEFAULT null,
  `injuries` varchar(64) DEFAULT null,
  `cards_given` varchar(64) DEFAULT null,
  `offsides` varchar(64) DEFAULT null,
  `penalties_missed` varchar(64) DEFAULT null,
  `create_at` timestamp DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);

CREATE TABLE `match_player` (
  `id` bigint(20) PRIMARY KEY AUTO_INCREMENT,
  `match_id` bigint(20),
  `team_type` ENUM ('home', 'away'),
  `player_id` bigint(20),
  `position_id` varchar(64),
  `starter` ENUM ('y', 'n'),
  `jersey_number` varchar(64) DEFAULT null,
  `create_at` timestamp DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);

CREATE TABLE `season_team` (
  `id` bigint(20) PRIMARY KEY AUTO_INCREMENT,
  `season_id` bigint(20),
  `team_id` bigint(20),
  `create_at` timestamp DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);

CREATE TABLE `team_player` (
  `id` bigint(20) PRIMARY KEY AUTO_INCREMENT,
  `team_id` bigint(20),
  `player_id` bigint(20),
  `jersey_number` varchar(64) DEFAULT null,
  `position_id` varchar(64),
  `create_at` timestamp DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);

CREATE TABLE `event_type` (
  `id` varchar(64) PRIMARY KEY,
  `create_at` timestamp DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);

CREATE TABLE `position` (
  `id` varchar(64) PRIMARY KEY,
  `create_at` timestamp DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);

CREATE TABLE `match_event` (
  `id` bigint(20) PRIMARY KEY AUTO_INCREMENT,
  `match_id` bigint(20),
  `event_type_id` varchar(64),
  `time` bigint(20) NOT NULL,
  `match_time` bigint(20) DEFAULT null,
  `match_clock` varchar(64) DEFAULT null,
  `team_type` ENUM ('home', 'away') DEFAULT null,
  `x` bigint(20) DEFAULT null,
  `y` bigint(20) DEFAULT null,
  `period` bigint(20) DEFAULT null,
  `period_type` ENUM ('regular_period', 'overtime', 'penalties', 'interrupted', 'pause') DEFAULT null,
  `outcome` varchar(64) DEFAULT null,
  `break_name` varchar(64) DEFAULT null,
  `create_at` timestamp DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);

CREATE TABLE `match_event_player` (
  `id` bigint(20) PRIMARY KEY AUTO_INCREMENT,
  `match_event_id` bigint(20),
  `player_id` bigint(20),
  `type` varchar(64) DEFAULT null,
  `create_at` timestamp DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);

CREATE TABLE `sport_translation` (
  `id` bigint(20) PRIMARY KEY AUTO_INCREMENT,
  `language` ENUM ('sqi', 'ar', 'hye', 'aze', 'bel', 'bs', 'br', 'bg', 'zh', 'zht', 'hr', 'cs', 'da', 'nl', 'en', 'et', 'fao', 'fi', 'fr', 'ka', 'de', 'el', 'heb', 'hu', 'isl', 'id', 'it', 'ja', 'kaz', 'ko', 'lv', 'lt', 'mk', 'ms', 'mol', 'me', 'no', 'pl', 'pt', 'ro', 'ru', 'sr', 'srl', 'sk', 'sl', 'es', 'sw', 'sv', 'th', 'tr', 'ukr', 'vi') NOT NULL,
  `sport_id` varchar(64),
  `name` varchar(64) DEFAULT null,
  `create_at` timestamp DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);

CREATE TABLE `league_translation` (
  `id` bigint(20) PRIMARY KEY AUTO_INCREMENT,
  `language` ENUM ('sqi', 'ar', 'hye', 'aze', 'bel', 'bs', 'br', 'bg', 'zh', 'zht', 'hr', 'cs', 'da', 'nl', 'en', 'et', 'fao', 'fi', 'fr', 'ka', 'de', 'el', 'heb', 'hu', 'isl', 'id', 'it', 'ja', 'kaz', 'ko', 'lv', 'lt', 'mk', 'ms', 'mol', 'me', 'no', 'pl', 'pt', 'ro', 'ru', 'sr', 'srl', 'sk', 'sl', 'es', 'sw', 'sv', 'th', 'tr', 'ukr', 'vi') NOT NULL,
  `league_id` bigint(20),
  `name` varchar(64) DEFAULT null,
  `create_at` timestamp DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);

CREATE TABLE `season_translation` (
  `id` bigint(20) PRIMARY KEY AUTO_INCREMENT,
  `language` ENUM ('sqi', 'ar', 'hye', 'aze', 'bel', 'bs', 'br', 'bg', 'zh', 'zht', 'hr', 'cs', 'da', 'nl', 'en', 'et', 'fao', 'fi', 'fr', 'ka', 'de', 'el', 'heb', 'hu', 'isl', 'id', 'it', 'ja', 'kaz', 'ko', 'lv', 'lt', 'mk', 'ms', 'mol', 'me', 'no', 'pl', 'pt', 'ro', 'ru', 'sr', 'srl', 'sk', 'sl', 'es', 'sw', 'sv', 'th', 'tr', 'ukr', 'vi') NOT NULL,
  `season_id` bigint(20),
  `name` varchar(64) NOT NULL,
  `create_at` timestamp DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);

CREATE TABLE `venue_translation` (
  `id` bigint(20) PRIMARY KEY AUTO_INCREMENT,
  `language` ENUM ('sqi', 'ar', 'hye', 'aze', 'bel', 'bs', 'br', 'bg', 'zh', 'zht', 'hr', 'cs', 'da', 'nl', 'en', 'et', 'fao', 'fi', 'fr', 'ka', 'de', 'el', 'heb', 'hu', 'isl', 'id', 'it', 'ja', 'kaz', 'ko', 'lv', 'lt', 'mk', 'ms', 'mol', 'me', 'no', 'pl', 'pt', 'ro', 'ru', 'sr', 'srl', 'sk', 'sl', 'es', 'sw', 'sv', 'th', 'tr', 'ukr', 'vi') NOT NULL,
  `venue_id` bigint(20),
  `name` varchar(64) NOT NULL,
  `city_name` varchar(64) DEFAULT null,
  `country_name` varchar(64) DEFAULT null,
  `create_at` timestamp DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);

CREATE TABLE `team_translation` (
  `id` bigint(20) PRIMARY KEY AUTO_INCREMENT,
  `language` ENUM ('sqi', 'ar', 'hye', 'aze', 'bel', 'bs', 'br', 'bg', 'zh', 'zht', 'hr', 'cs', 'da', 'nl', 'en', 'et', 'fao', 'fi', 'fr', 'ka', 'de', 'el', 'heb', 'hu', 'isl', 'id', 'it', 'ja', 'kaz', 'ko', 'lv', 'lt', 'mk', 'ms', 'mol', 'me', 'no', 'pl', 'pt', 'ro', 'ru', 'sr', 'srl', 'sk', 'sl', 'es', 'sw', 'sv', 'th', 'tr', 'ukr', 'vi') NOT NULL,
  `team_id` bigint(20),
  `name` varchar(64) NOT NULL,
  `country` varchar(64) DEFAULT null,
  `create_at` timestamp DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);

CREATE TABLE `match_status_translation` (
  `id` bigint(20) PRIMARY KEY AUTO_INCREMENT,
  `language` ENUM ('sqi', 'ar', 'hye', 'aze', 'bel', 'bs', 'br', 'bg', 'zh', 'zht', 'hr', 'cs', 'da', 'nl', 'en', 'et', 'fao', 'fi', 'fr', 'ka', 'de', 'el', 'heb', 'hu', 'isl', 'id', 'it', 'ja', 'kaz', 'ko', 'lv', 'lt', 'mk', 'ms', 'mol', 'me', 'no', 'pl', 'pt', 'ro', 'ru', 'sr', 'srl', 'sk', 'sl', 'es', 'sw', 'sv', 'th', 'tr', 'ukr', 'vi') NOT NULL,
  `match_status_id` varchar(64),
  `name` varchar(64) NOT NULL,
  `create_at` timestamp DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);

CREATE TABLE `player_translation` (
  `id` bigint(20) PRIMARY KEY AUTO_INCREMENT,
  `language` ENUM ('sqi', 'ar', 'hye', 'aze', 'bel', 'bs', 'br', 'bg', 'zh', 'zht', 'hr', 'cs', 'da', 'nl', 'en', 'et', 'fao', 'fi', 'fr', 'ka', 'de', 'el', 'heb', 'hu', 'isl', 'id', 'it', 'ja', 'kaz', 'ko', 'lv', 'lt', 'mk', 'ms', 'mol', 'me', 'no', 'pl', 'pt', 'ro', 'ru', 'sr', 'srl', 'sk', 'sl', 'es', 'sw', 'sv', 'th', 'tr', 'ukr', 'vi') NOT NULL,
  `player_id` bigint(20),
  `name` varchar(64) NOT NULL,
  `create_at` timestamp DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);

CREATE TABLE `event_type_translation` (
  `id` bigint(20) PRIMARY KEY AUTO_INCREMENT,
  `language` ENUM ('sqi', 'ar', 'hye', 'aze', 'bel', 'bs', 'br', 'bg', 'zh', 'zht', 'hr', 'cs', 'da', 'nl', 'en', 'et', 'fao', 'fi', 'fr', 'ka', 'de', 'el', 'heb', 'hu', 'isl', 'id', 'it', 'ja', 'kaz', 'ko', 'lv', 'lt', 'mk', 'ms', 'mol', 'me', 'no', 'pl', 'pt', 'ro', 'ru', 'sr', 'srl', 'sk', 'sl', 'es', 'sw', 'sv', 'th', 'tr', 'ukr', 'vi') NOT NULL,
  `event_type_id` varchar(64),
  `name` varchar(64) NOT NULL,
  `create_at` timestamp DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);

CREATE TABLE `position_translation` (
  `id` bigint(20) PRIMARY KEY AUTO_INCREMENT,
  `language` ENUM ('sqi', 'ar', 'hye', 'aze', 'bel', 'bs', 'br', 'bg', 'zh', 'zht', 'hr', 'cs', 'da', 'nl', 'en', 'et', 'fao', 'fi', 'fr', 'ka', 'de', 'el', 'heb', 'hu', 'isl', 'id', 'it', 'ja', 'kaz', 'ko', 'lv', 'lt', 'mk', 'ms', 'mol', 'me', 'no', 'pl', 'pt', 'ro', 'ru', 'sr', 'srl', 'sk', 'sl', 'es', 'sw', 'sv', 'th', 'tr', 'ukr', 'vi') NOT NULL,
  `position_id` varchar(64),
  `name` varchar(64) NOT NULL,
  `create_at` timestamp DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);

ALTER TABLE `league` ADD FOREIGN KEY (`sport_id`) REFERENCES `sport` (`id`);

ALTER TABLE `season` ADD FOREIGN KEY (`league_id`) REFERENCES `league` (`id`);

ALTER TABLE `team` ADD FOREIGN KEY (`venue_id`) REFERENCES `venue` (`id`);

ALTER TABLE `match` ADD FOREIGN KEY (`sport_id`) REFERENCES `sport` (`id`);

ALTER TABLE `match` ADD FOREIGN KEY (`league_id`) REFERENCES `league` (`id`);

ALTER TABLE `match` ADD FOREIGN KEY (`season_id`) REFERENCES `season` (`id`);

ALTER TABLE `match` ADD FOREIGN KEY (`match_status_id`) REFERENCES `match_status` (`id`);

ALTER TABLE `match` ADD FOREIGN KEY (`venue_id`) REFERENCES `venue` (`id`);

ALTER TABLE `player` ADD FOREIGN KEY (`position_id`) REFERENCES `position` (`id`);

ALTER TABLE `match_score` ADD FOREIGN KEY (`match_id`) REFERENCES `match` (`id`);

ALTER TABLE `match_team` ADD FOREIGN KEY (`match_id`) REFERENCES `match` (`id`);

ALTER TABLE `match_team` ADD FOREIGN KEY (`team_id`) REFERENCES `team` (`id`);

ALTER TABLE `match_team_stats` ADD FOREIGN KEY (`match_team_id`) REFERENCES `match_team` (`id`);

ALTER TABLE `match_player` ADD FOREIGN KEY (`match_id`) REFERENCES `match` (`id`);

ALTER TABLE `match_player` ADD FOREIGN KEY (`player_id`) REFERENCES `player` (`id`);

ALTER TABLE `match_player` ADD FOREIGN KEY (`position_id`) REFERENCES `position` (`id`);

ALTER TABLE `season_team` ADD FOREIGN KEY (`season_id`) REFERENCES `season` (`id`);

ALTER TABLE `season_team` ADD FOREIGN KEY (`team_id`) REFERENCES `team` (`id`);

ALTER TABLE `team_player` ADD FOREIGN KEY (`team_id`) REFERENCES `team` (`id`);

ALTER TABLE `team_player` ADD FOREIGN KEY (`player_id`) REFERENCES `player` (`id`);

ALTER TABLE `team_player` ADD FOREIGN KEY (`position_id`) REFERENCES `position` (`id`);

ALTER TABLE `match_event` ADD FOREIGN KEY (`match_id`) REFERENCES `match` (`id`);

ALTER TABLE `match_event` ADD FOREIGN KEY (`event_type_id`) REFERENCES `event_type` (`id`);

ALTER TABLE `match_event_player` ADD FOREIGN KEY (`match_event_id`) REFERENCES `match_event` (`id`);

ALTER TABLE `match_event_player` ADD FOREIGN KEY (`player_id`) REFERENCES `player` (`id`);

ALTER TABLE `sport_translation` ADD FOREIGN KEY (`sport_id`) REFERENCES `sport` (`id`);

ALTER TABLE `league_translation` ADD FOREIGN KEY (`league_id`) REFERENCES `league` (`id`);

ALTER TABLE `season_translation` ADD FOREIGN KEY (`season_id`) REFERENCES `season` (`id`);

ALTER TABLE `venue_translation` ADD FOREIGN KEY (`venue_id`) REFERENCES `venue` (`id`);

ALTER TABLE `team_translation` ADD FOREIGN KEY (`team_id`) REFERENCES `team` (`id`);

ALTER TABLE `match_status_translation` ADD FOREIGN KEY (`match_status_id`) REFERENCES `match_status` (`id`);

ALTER TABLE `player_translation` ADD FOREIGN KEY (`player_id`) REFERENCES `player` (`id`);

ALTER TABLE `event_type_translation` ADD FOREIGN KEY (`event_type_id`) REFERENCES `event_type` (`id`);

ALTER TABLE `position_translation` ADD FOREIGN KEY (`position_id`) REFERENCES `position` (`id`);

ALTER TABLE `league` COMMENT = "    - The API doesn't offer the enough information, so it must be add information by self.

## API_Source:
    - Sportsradar: competition_info";

ALTER TABLE `team` COMMENT = "    - It records all teams.

### API_Source:
    - Sportsradar: competitor_profile";

ALTER TABLE `match` COMMENT = "    - many-to-many table, It refers from league and league_match_info table.

## API Source:
    - Sportsradar: seaon_schedule";

ALTER TABLE `player` COMMENT = "### API Source:
    - Sportsradar: player_profile";
