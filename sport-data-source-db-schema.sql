CREATE TABLE `source_sport` (
  `id` bigint(20) PRIMARY KEY AUTO_INCREMENT,
  `vendor` ENUM ('sportradar', 'bti', 'nami') NOT NULL,
  `source_id` varchar(64) NOT NULL,
  `sport_id` varchar(64) DEFAULT null,
  `create_at` timestamp DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);

CREATE TABLE `source_league` (
  `id` bigint(20) PRIMARY KEY AUTO_INCREMENT,
  `vendor` ENUM ('sportradar', 'bti', 'nami') NOT NULL,
  `source_id` varchar(64) NOT NULL,
  `league_id` bigint(20) NOT NULL,
  `sport_id` varchar(64) DEFAULT null,
  `name` varchar(64) DEFAULT null,
  `logo` varchar(255) DEFAULT null,
  `confederation` varchar(64) DEFAULT null,
  `country_code` varchar(64) DEFAULT null,
  `create_at` timestamp DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);

CREATE TABLE `source_season` (
  `id` bigint(20) PRIMARY KEY AUTO_INCREMENT,
  `vendor` ENUM ('sportradar', 'bti', 'nami') NOT NULL,
  `source_id` varchar(64) NOT NULL,
  `season_id` bigint(20) DEFAULT null,
  `league_id` varchar(64) DEFAULT null,
  `name` varchar(64) DEFAULT null,
  `start_at` bigint(20) NOT NULL,
  `end_at` bigint(20) NOT NULL,
  `year` varchar(64) NOT NULL,
  `create_at` timestamp DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);

CREATE TABLE `source_venue` (
  `id` bigint(20) PRIMARY KEY AUTO_INCREMENT,
  `vendor` ENUM ('sportradar', 'bti', 'nami') NOT NULL,
  `source_id` varchar(64) NOT NULL,
  `venue_id` bigint(20) DEFAULT null,
  `name` varchar(64) DEFAULT null,
  `capacity` bigint(20) DEFAULT null,
  `map_coordinates` varchar(64) DEFAULT null,
  `country_code` varchar(64) DEFAULT null,
  `create_at` timestamp DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);

CREATE TABLE `source_team` (
  `id` bigint(20) PRIMARY KEY AUTO_INCREMENT,
  `vendor` ENUM ('sportradar', 'bti', 'nami') NOT NULL,
  `source_id` varchar(64) NOT NULL,
  `team_id` bigint(20) DEFAULT null,
  `name` varchar(64) DEFAULT null,
  `country_code` varchar(64) DEFAULT null,
  `abbreviation` varchar(64) DEFAULT null,
  `logo` varchar(255) DEFAULT null,
  `venue_id` varchar(64) DEFAULT null,
  `gender` varchar(64) DEFAULT null,
  `create_at` timestamp DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);

CREATE TABLE `source_match` (
  `id` bigint(20) PRIMARY KEY AUTO_INCREMENT,
  `vendor` ENUM ('sportradar', 'bti', 'nami') NOT NULL,
  `source_id` varchar(64) NOT NULL,
  `match_id` bigint(20) DEFAULT null,
  `sport_id` varchar(64) DEFAULT null,
  `league_id` varchar(64) DEFAULT null,
  `season_id` varchar(64) DEFAULT null,
  `round` bigint(20) DEFAULT null,
  `match_status_id` varchar(64) DEFAULT null,
  `start_at` bigint(20) NOT NULL,
  `played` varchar(64) DEFAULT null,
  `remaining` varchar(64) DEFAULT null,
  `venue_id` varchar(64) DEFAULT null,
  `create_at` timestamp DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);

CREATE TABLE `source_match_status` (
  `id` bigint(20) PRIMARY KEY AUTO_INCREMENT,
  `vendor` ENUM ('sportradar', 'bti', 'nami') NOT NULL,
  `source_id` varchar(64) NOT NULL,
  `match_status_id` varchar(64) NOT NULL,
  `create_at` timestamp DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);

CREATE TABLE `source_player` (
  `id` bigint(20) PRIMARY KEY AUTO_INCREMENT,
  `vendor` ENUM ('sportradar', 'bti', 'nami') NOT NULL,
  `source_id` varchar(64) NOT NULL,
  `player_id` bigint(20) DEFAULT null,
  `name` varchar(64) DEFAULT null,
  `logo` varchar(255) DEFAULT null,
  `position_id` varchar(64) DEFAULT null,
  `date_of_birth` varchar(64) DEFAULT null,
  `nationality` varchar(64) DEFAULT null,
  `country_code` varchar(64) DEFAULT null,
  `height` varchar(64) DEFAULT null,
  `weight` varchar(64) DEFAULT null,
  `jersey_number` varchar(64) DEFAULT null,
  `preferred_foot` varchar(64) DEFAULT null,
  `place_of_birth` varchar(64) DEFAULT null,
  `gender` varchar(64) DEFAULT null,
  `create_at` timestamp DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);

CREATE TABLE `source_match_score` (
  `id` bigint(20) PRIMARY KEY AUTO_INCREMENT,
  `vendor` ENUM ('sportradar', 'bti', 'nami') NOT NULL,
  `match_score_id` bigint(20) DEFAULT null,
  `match_id` varchar(64) NOT NULL,
  `team_type` ENUM ('home', 'away'),
  `period_type` ENUM ('regular_period', 'overtime', 'entire'),
  `order` bigint(20) DEFAULT 0,
  `score` int(10) DEFAULT 0,
  `create_at` timestamp DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);

CREATE TABLE `source_match_team` (
  `id` bigint(20) PRIMARY KEY AUTO_INCREMENT,
  `vendor` ENUM ('sportradar', 'bti', 'nami') NOT NULL,
  `match_id` varchar(64) NOT NULL,
  `team_type` ENUM ('home', 'away') NOT NULL,
  `match_team_id` bigint(20) DEFAULT null,
  `team_id` varchar(64) DEFAULT null,
  `create_at` timestamp DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);

CREATE TABLE `source_match_team_stats` (
  `id` bigint(20) PRIMARY KEY AUTO_INCREMENT,
  `vendor` ENUM ('sportradar', 'bti', 'nami') NOT NULL,
  `match_id` varchar(64) NOT NULL,
  `team_type` ENUM ('home', 'away') NOT NULL,
  `match_team_stats_id` bigint(20) DEFAULT null,
  `assists` varchar(64) DEFAULT null,
  `rebounds` varchar(64) DEFAULT null,
  `three_point_attempts_successful` varchar(64) DEFAULT null,
  `three_point_attempts_total` varchar(64) DEFAULT null,
  `two_point_attempts_successful` varchar(64) DEFAULT null,
  `two_point_attempts_total` varchar(64) DEFAULT null,
  `free_throw_attempts_successful` varchar(64) DEFAULT null,
  `free_throw_attempts_total` varchar(64) DEFAULT null,
  `offensive_rebounds` varchar(64) DEFAULT null,
  `defensive_rebounds` varchar(64) DEFAULT null,
  `turnovers` varchar(64) DEFAULT null,
  `steals` varchar(64) DEFAULT null,
  `shots_blocked` varchar(64) DEFAULT null,
  `fouls` varchar(64) DEFAULT null,
  `team_rebounds` varchar(64) DEFAULT null,
  `team_turnovers` varchar(64) DEFAULT null,
  `timeouts` varchar(64) DEFAULT null,
  `yellow_cards` varchar(64) DEFAULT null,
  `yellow_red_cards` varchar(64) DEFAULT null,
  `red_cards` varchar(64) DEFAULT null,
  `corner_kicks` varchar(64) DEFAULT null,
  `substitutions` varchar(64) DEFAULT null,
  `ball_possession` varchar(64) DEFAULT null,
  `free_kicks` varchar(64) DEFAULT null,
  `goal_kicks` varchar(64) DEFAULT null,
  `throw_ins` varchar(64) DEFAULT null,
  `shots_on_target` varchar(64) DEFAULT null,
  `shots_total` varchar(64) DEFAULT null,
  `shots_off_target` varchar(64) DEFAULT null,
  `shots_saved` varchar(64) DEFAULT null,
  `injuries` varchar(64) DEFAULT null,
  `cards_given` varchar(64) DEFAULT null,
  `offsides` varchar(64) DEFAULT null,
  `penalties_missed` varchar(64) DEFAULT null,
  `create_at` timestamp DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);

CREATE TABLE `source_match_player` (
  `id` bigint(20) PRIMARY KEY AUTO_INCREMENT,
  `vendor` ENUM ('sportradar', 'bti', 'nami') NOT NULL,
  `match_id` varchar(64) NOT NULL,
  `player_id` varchar(64) NOT NULL,
  `match_player_id` bigint(20) DEFAULT null,
  `team_type` ENUM ('home', 'away'),
  `position_id` varchar(64) DEFAULT null,
  `starter` ENUM ('y', 'n'),
  `jersey_number` varchar(64) DEFAULT null,
  `create_at` timestamp DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);

CREATE TABLE `source_season_team` (
  `id` bigint(20) PRIMARY KEY AUTO_INCREMENT,
  `vendor` ENUM ('sportradar', 'bti', 'nami') NOT NULL,
  `season_id` varchar(64) NOT NULL,
  `team_id` varchar(64) NOT NULL,
  `source_team_id` bigint(20) DEFAULT null,
  `create_at` timestamp DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);

CREATE TABLE `source_team_player` (
  `id` bigint(20) PRIMARY KEY AUTO_INCREMENT,
  `vendor` ENUM ('sportradar', 'bti', 'nami') NOT NULL,
  `team_id` varchar(64) NOT NULL,
  `player_id` varchar(64) NOT NULL,
  `team_player_id` bigint(20) DEFAULT null,
  `jersey_number` varchar(64) DEFAULT null,
  `position_id` varchar(64) DEFAULT null,
  `create_at` timestamp DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);

CREATE TABLE `source_event_type` (
  `id` bigint(20) PRIMARY KEY AUTO_INCREMENT,
  `vendor` ENUM ('sportradar', 'bti', 'nami') NOT NULL,
  `source_id` varchar(64) NOT NULL,
  `event_type_id` varchar(64) DEFAULT null,
  `create_at` timestamp DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);

CREATE TABLE `source_position` (
  `id` bigint(20) PRIMARY KEY AUTO_INCREMENT,
  `vendor` ENUM ('sportradar', 'bti', 'nami') NOT NULL,
  `source_id` varchar(64) NOT NULL,
  `position_id` varchar(64) DEFAULT null,
  `create_at` timestamp DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);

CREATE TABLE `source_match_event` (
  `id` bigint(20) PRIMARY KEY AUTO_INCREMENT,
  `vendor` ENUM ('sportradar', 'bti', 'nami') NOT NULL,
  `source_id` varchar(64) NOT NULL,
  `match_event_id` bigint(20) DEFAULT null,
  `match_id` varchar(64) DEFAULT null,
  `event_type_id` varchar(64) DEFAULT null,
  `time` bigint(20) NOT NULL,
  `match_time` bigint(20) DEFAULT null,
  `match_clock` varchar(64) DEFAULT null,
  `team_type` ENUM ('home', 'away') DEFAULT null,
  `x` bigint(20) DEFAULT null,
  `y` bigint(20) DEFAULT null,
  `period` bigint(20) DEFAULT null,
  `period_type` ENUM ('regular_period', 'overtime', 'penalties', 'interrupted', 'pause') DEFAULT null,
  `outcome` varchar(64) DEFAULT null,
  `break_name` varchar(64) DEFAULT null,
  `create_at` timestamp DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);

CREATE TABLE `source_match_event_player` (
  `id` bigint(20) PRIMARY KEY AUTO_INCREMENT,
  `vendor` ENUM ('sportradar', 'bti', 'nami') NOT NULL,
  `source_event_id` varchar(64) NOT NULL,
  `player_id` varchar(64) NOT NULL,
  `match_event_player_id` bigint(20) DEFAULT null,
  `type` varchar(64) DEFAULT null,
  `create_at` timestamp DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);

CREATE TABLE `source_sport_translation` (
  `id` bigint(20) PRIMARY KEY AUTO_INCREMENT,
  `language` ENUM ('sqi', 'ar', 'hye', 'aze', 'bel', 'bs', 'br', 'bg', 'zh', 'zht', 'hr', 'cs', 'da', 'nl', 'en', 'et', 'fao', 'fi', 'fr', 'ka', 'de', 'el', 'heb', 'hu', 'isl', 'id', 'it', 'ja', 'kaz', 'ko', 'lv', 'lt', 'mk', 'ms', 'mol', 'me', 'no', 'pl', 'pt', 'ro', 'ru', 'sr', 'srl', 'sk', 'sl', 'es', 'sw', 'sv', 'th', 'tr', 'ukr', 'vi') NOT NULL,
  `source_sport_id` bigint(20),
  `name` varchar(64) DEFAULT null,
  `create_at` timestamp DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);

CREATE TABLE `source_league_translation` (
  `id` bigint(20) PRIMARY KEY AUTO_INCREMENT,
  `language` ENUM ('sqi', 'ar', 'hye', 'aze', 'bel', 'bs', 'br', 'bg', 'zh', 'zht', 'hr', 'cs', 'da', 'nl', 'en', 'et', 'fao', 'fi', 'fr', 'ka', 'de', 'el', 'heb', 'hu', 'isl', 'id', 'it', 'ja', 'kaz', 'ko', 'lv', 'lt', 'mk', 'ms', 'mol', 'me', 'no', 'pl', 'pt', 'ro', 'ru', 'sr', 'srl', 'sk', 'sl', 'es', 'sw', 'sv', 'th', 'tr', 'ukr', 'vi') NOT NULL,
  `source_league_id` bigint(20),
  `name` varchar(64) DEFAULT null,
  `create_at` timestamp DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);

CREATE TABLE `source_season_translation` (
  `id` bigint(20) PRIMARY KEY AUTO_INCREMENT,
  `language` ENUM ('sqi', 'ar', 'hye', 'aze', 'bel', 'bs', 'br', 'bg', 'zh', 'zht', 'hr', 'cs', 'da', 'nl', 'en', 'et', 'fao', 'fi', 'fr', 'ka', 'de', 'el', 'heb', 'hu', 'isl', 'id', 'it', 'ja', 'kaz', 'ko', 'lv', 'lt', 'mk', 'ms', 'mol', 'me', 'no', 'pl', 'pt', 'ro', 'ru', 'sr', 'srl', 'sk', 'sl', 'es', 'sw', 'sv', 'th', 'tr', 'ukr', 'vi') NOT NULL,
  `source_season_id` bigint(20),
  `name` varchar(64) NOT NULL,
  `create_at` timestamp DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);

CREATE TABLE `source_venue_translation` (
  `id` bigint(20) PRIMARY KEY AUTO_INCREMENT,
  `language` ENUM ('sqi', 'ar', 'hye', 'aze', 'bel', 'bs', 'br', 'bg', 'zh', 'zht', 'hr', 'cs', 'da', 'nl', 'en', 'et', 'fao', 'fi', 'fr', 'ka', 'de', 'el', 'heb', 'hu', 'isl', 'id', 'it', 'ja', 'kaz', 'ko', 'lv', 'lt', 'mk', 'ms', 'mol', 'me', 'no', 'pl', 'pt', 'ro', 'ru', 'sr', 'srl', 'sk', 'sl', 'es', 'sw', 'sv', 'th', 'tr', 'ukr', 'vi') NOT NULL,
  `source_venue_id` bigint(20),
  `name` varchar(64) NOT NULL,
  `city_name` varchar(64) DEFAULT null,
  `country_name` varchar(64) DEFAULT null,
  `create_at` timestamp DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);

CREATE TABLE `source_team_translation` (
  `id` bigint(20) PRIMARY KEY AUTO_INCREMENT,
  `language` ENUM ('sqi', 'ar', 'hye', 'aze', 'bel', 'bs', 'br', 'bg', 'zh', 'zht', 'hr', 'cs', 'da', 'nl', 'en', 'et', 'fao', 'fi', 'fr', 'ka', 'de', 'el', 'heb', 'hu', 'isl', 'id', 'it', 'ja', 'kaz', 'ko', 'lv', 'lt', 'mk', 'ms', 'mol', 'me', 'no', 'pl', 'pt', 'ro', 'ru', 'sr', 'srl', 'sk', 'sl', 'es', 'sw', 'sv', 'th', 'tr', 'ukr', 'vi') NOT NULL,
  `source_team_id` bigint(20),
  `name` varchar(64) NOT NULL,
  `country` varchar(64) DEFAULT null,
  `create_at` timestamp DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);

CREATE TABLE `source_player_translation` (
  `id` bigint(20) PRIMARY KEY AUTO_INCREMENT,
  `language` ENUM ('sqi', 'ar', 'hye', 'aze', 'bel', 'bs', 'br', 'bg', 'zh', 'zht', 'hr', 'cs', 'da', 'nl', 'en', 'et', 'fao', 'fi', 'fr', 'ka', 'de', 'el', 'heb', 'hu', 'isl', 'id', 'it', 'ja', 'kaz', 'ko', 'lv', 'lt', 'mk', 'ms', 'mol', 'me', 'no', 'pl', 'pt', 'ro', 'ru', 'sr', 'srl', 'sk', 'sl', 'es', 'sw', 'sv', 'th', 'tr', 'ukr', 'vi') NOT NULL,
  `source_player_id` bigint(20),
  `name` varchar(64) NOT NULL,
  `create_at` timestamp DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);

ALTER TABLE `source_sport_translation` ADD FOREIGN KEY (`source_sport_id`) REFERENCES `source_sport` (`id`);

ALTER TABLE `source_league_translation` ADD FOREIGN KEY (`source_league_id`) REFERENCES `source_league` (`id`);

ALTER TABLE `source_season_translation` ADD FOREIGN KEY (`source_season_id`) REFERENCES `source_season` (`id`);

ALTER TABLE `source_venue_translation` ADD FOREIGN KEY (`source_venue_id`) REFERENCES `source_venue` (`id`);

ALTER TABLE `source_team_translation` ADD FOREIGN KEY (`source_team_id`) REFERENCES `source_team` (`id`);

ALTER TABLE `source_player_translation` ADD FOREIGN KEY (`source_player_id`) REFERENCES `source_player` (`id`);
