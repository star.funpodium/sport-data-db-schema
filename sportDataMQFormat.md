## common format
``` json
{
    "vendor":"vendor",
    "type": "type",
    "language": "language",
    "data":[
        {
            "...":"..."
        }
    ],
    "generated_at": 1056784329013 //timestamp
}
```
## vendor
- sportradar
- bti
- nami
## language
- sqi
- ar
- hye
- aze
- bel
- bs
- br
- bg
- zh
- zht
- hr
- cs
- da
- nl
- en
- et
- fao
- fi
- fr
- ka
- de
- el
- heb
- hu
- isl
- id
- it
- ja
- kaz
- ko
- lv
- lt
- mk
- ms
- mol
- me
- no
- pl
- pt
- ro
- ru
- sr
- srl
- sk
- sl
- es
- sw
- sv
- th
- tr
- ukr
- vi

## Data object format

### sport
```json
{
    "id": "string", //not null
    "name": "string"
}
```

### league
```json
{
    "id": "string",// not null
    "name": "string",
    "sport_id": "string",
    "logo": "string",
    "confederation": "string",
    "country_code": "string"
}
```

### season
```json
{
    "id": "string",// not null
    "league_id": "string",// not null
    "name": "string",
    "start_at": 1056784329013, // timestamp
    "end_at": 1056784329013, //timestamp
    "year": "string"
    
}
```
### stage
```json
{
    "id": "string", // not null
    "type": "stage_type",
    "season_id": "string",// not null
    "phase_id": "string",//not null
    "start_at": 1056784329013, // timestamp
    "end_at": 1056784329013, // timestamp
}
```
### group
```json
{
    "id": "string", //not null
    "stage_id": "string", //not null
    "name": "string",
    "max_rounds": "string",

}
```
### group_team
```json
{
    "group_id": "string", //not null
    "team_id": "string", //not null
}
```
### group_standing
```json
{
    "group_id": "string", //not null
    "team_id": "string", //not null
    "type": "group_standing_type", //not null
    "rank": "string",//football,basketball
    "played": "string",//football,basketball
    "win": "string",//football,basketball
    "loss": "string",//football,basketball
    "draw": "string",//football,basketball
    "goals_for": "string",//football
    "goals_against": "string",//football
    "goals_diff": "string",//football
    "points": "string",//football,basketball
    "change": "string",//football,basketball
    "points_per_game": "string",//football
    "points_for": "string",//football,basketball
    "points_against": "string", // football,basketball
    "games_behind": "string", //football,basketball
    "last_ten_win_record": "string", //football,basketball
    "last_ten_loss_record": "string", //football,basketball
    "current_outcome": "string", // basketball
    "losses_conference": "string", // basketball
    "losses_division": "string", // basketball
    "streak": "string", // basketball
    "wins_conference": "string", // basketball
    "wins_division": "string", // basketball
}
```

### venue
```json
{
    "id": "string",// not null
    "name": "string",
    "city_name": "string",
    "country_code": "string",
    "capacity": "string",
    "map_coordinates": "string"
}
```

### team
```json
{
    "id": "string",// not null
    "name": "string",
    "league_id": "string",
    "country_code": "string",
    "abbreviation": "string",
    "logo": "string",
    "venue_id": "string",
    "gender": "string"
}
```
### player
```json
{
    "id": "string",// not null
    "name": "string",
    "logo": "string",
    "position_id": "string",
    "team_id": "string",
    "date_of_birth": "string",
    "nationality": "string",
    "country_code": "string",
    "height": "string",
    "weight": "string",
    "jersey_number": 10, //int
    "preferred_foot": "string",
    "place_of_birth": "string",
    "gender": "string"
}
```
### match
```json
{
    "id": "string",// not null
    "sport_id": "string",//not null
    "league_id": "string",// not null
    "season_id": "string",// not null
    "stage_id": "string",
    "group_id": "string",
    "round": "10",
    "match_status_id": "string",
    "played": "string",
    "remaining": "string",
    "start_at": 1564317895183, //timstamp,
    "venue_id": "string",
    "teams":[
        {
            "team_id":"string",// not null
            "team_type": "team_type" // enum,not null
        }
    ]
}
```
### match_score
```json
{
    "match_id": "string",// not null
    "team_type": "team_type",//enum,not null
    "period_type": "match_score_period_type",//enum,not null
    "order": 0,//int,not null
    "score": 10//int,not null
}
```

### match_team_stats
```json
{
    "match_id": "string",// not null
    "team_type": "team_type",//enum, not null
    "assists": "string",  // basketball
    "rebounds": "string",  // basketball
    "three_point_attempts_successful": "string",  // basketball
    "three_point_attempts_total": "string",  // basketball
    "two_point_attempts_successful": "string",  // basketball
    "two_point_attempts_total": "string",  // basketball
    "free_throw_attempts_successful": "string",  // basketball
    "free_throw_attempts_total": "string",  // basketball
    "offensive_rebounds": "string",  // basketball
    "defensive_rebounds": "string",  // basketball
    "turnovers": "string",  // basketball
    "steals": "string",  // basketball
    "shots_blocked": "string",  // basketball,football
    "fouls": "string",  // basketball,football
    "team_rebounds": "string",  // basketball
    "team_turnovers": "string",  // basketball
    "timeouts": "string", //basketball
    "yellow_cards": "string",  // football
    "yellow_red_cards": "string",  // football
    "red_cards": "string",  // football
    "corner_kicks": "string",  // football
    "substitutions": "string",  // football
    "ball_possession": "string",  // basketball,football
    "free_kicks": "string",  // football
    "goal_kicks": "string",  // football
    "throw_ins": "string",  // football
    "shots_on_target": "string",  // football
    "shots_total": "string",  // football
    "shots_off_target": "string",  // football
    "shots_saved": "string",  // football
    "injuries": "string",  // football
    "cards_given": "string", //football
    "offsides": "string", //football
    "penalties_missed": "string", //football
}
```
### match_player
```json
{
    "match_id": "string",// not null
    "player_id": "string",// not null
    "team_type": "team_type", //enum, not null
    "position_id": "string",
    "starter": "bool_type", //enum
    "jersey_number": 10, //int
}
```
### match_player_stats
```json
{
    "match_id": "string",// not null
    "player_id": "string",// not null
    "assists": "string",//basketball,football
    "blocks": "string",//basketball
    "defensive_rebounds": "string",//basketball
    "field_goals_attempted": "string",//basketball
    "field_goals_made": "string",//basketball
    "free_throws_attempted": "string",//basketball
    "free_throws_made": "string",//basketball
    "minutes": "string",//basketball
    "offensive_rebounds": "string",//basketball
    "personal_fouls": "string",//basketball
    "points": "string",//basketball
    "steals": "string",//basketball
    "technical_fouls": "string",//basketball
    "three_pointers_attempted": "string",//basketball
    "three_pointers_made": "string",//basketball
    "total_rebounds": "string",//basketball
    "turnovers": "string",//basketball
    "chances_created": "string",//football
    "clearances": "string",//football
    "corner_kicks": "string",//football
    "crosses_successful": "string",//football
    "crosses_total": "string",//football
    "defensive_blocks": "string",//football
    "diving_saves": "string",//football
    "dribbles_completed": "string",//football
    "fouls_committed": "string",//football
    "goals_by_head": "string",//football
    "goals_by_penalty": "string",//football
    "goals_conceded": "string",//football
    "goals_scored": "string",//football
    "interceptions": "string",//football
    "long_passes_successful": "string",//football
    "long_passes_total": "string",//football
    "long_passes_unsuccessful": "string",//football
    "loss_of_possession": "string",//football
    "minutes_played": "string",//football
    "offsides": "string",//football
    "own_goals": "string",//football
    "passes_successful": "string",//football
    "passes_total": "string",//football
    "passes_unsuccessful": "string",//football
    "penalties_faced": "string",//football
    "penalties_missed": "string",//football
    "penalties_saved": "string",//football
    "red_cards": "string",//football
    "shots_blocked": "string",//football
    "shots_faced_saved": "string",//football
    "shots_faced_total": "string",//football
    "shots_off_target": "string",//football
    "shots_on_target": "string",//football
    "substituted_in": "string",//football
    "substituted_out": "string",//football
    "tackles_successful": "string",//football
    "tackles_total": "string",//football
    "was_fouled": "string",//football
    "yellow_cards": "string",//football
    "yellow_red_cards": "string",//football
}
```
### season_leader
```json
{
    "season_id": "string",//not null
    "season_leader_type_id": "string",//not null
    "rank": "string",//not null
    "player_id": "string",
    "value": "string"
}
```
### season_team
```json
{
    "season_id": "string",//not null
    "team_id": "string"//not null
}
```
### season_team_stats
```json
{
    "season_id": "string",//not null
    "team_id": "string",//not null
    "shots_on_target": "string",//football
    "shots_total": "string",//football
    "shots_off_target": "string",//football
    "corner_kicks": "string",//football
    "average_ball_possession": "string",//football
    "shots_blocked": "string",//football
    "free_kicks": "string",//football
    "offsides": "string",//football
    "matches_played": "string",//football
    "shots_on_post": "string",//football
    "shots_on_bar": "string",//football
    "goals_by_foot": "string",//football
    "goals_by_head": "string",//football
    "yellow_cards": "string",//football
    "cards_given": "string",//football
    "red_cards": "string",//football
    "goals_scored": "string",//football
    "goals_conceded": "string",//football
    "yellow_red_cards": "string",//football
    "penalties_missed": "string",//football
    "goals_scored_first_half": "string",//football
    "goals_scored_second_half": "string",//football
    "goals_conceded_first_half": "string",//football
    "goals_conceded_second_half": "string",//football
}
```
### season_player_stats
```json
{
    "season_id": "string",//not null
    "team_id": "string",//not null
    "player_id": "string",//not null
    "assists": "string",//football
    "cards_given": "string",//football
    "chances_created": "string",//football
    "clearances": "string",//football
    "corner_kicks": "string",//football
    "crosses_successful": "string",//football
    "crosses_total": "string",//football
    "dribbles_completed": "string",//football
    "goals_by_head": "string",//football
    "goals_by_penalty": "string",//football
    "goals_conceded": "string",//football
    "goals_scored": "string",//football
    "interceptions": "string",//football
    "long_passes_successful": "string",//football
    "long_passes_total": "string",//football
    "long_passes_unsuccessful": "string",//football
    "loss_of_possession": "string",//football
    "matches_played": "string",//football
    "minutes_played": "string",//football
    "offsides": "string",//football
    "own_goals": "string",//football
    "passes_successful": "string",//football
    "passes_total": "string",//football
    "passes_unsuccessful": "string",//football
    "penalties_faced": "string",//football
    "penalties_missed": "string",//football
    "penalties_saved": "string",//football
    "red_cards": "string",//football
    "shots_blocked": "string",//football
    "shots_faced": "string",//football
    "shots_off_target": "string",//football
    "shots_on_target": "string",//football
    "substituted_in": "string",//football
    "substituted_out": "string",//football
    "tackles_successful": "string",//football
    "tackles_total": "string",//football
    "yellow_cards": "string",//football
    "yellow_red_cards": "string",//football
}
```
### team_player
```json
{
    "team_id": "string",//not null
    "players": [
        {
            "player_id":"string", //not null
            "jersey_number": "string",
            "position_id": "string"
        }
    ]
}
```
### match_event
```json
{
    "id": "string",//not null
    "match_id": "string",//not null
    "event_type_id": "string",//not null
    "order": 1,//not null
    "match_time":32,//int,not null
    "match_clock": "string",//not null
    "team_type": "team_type",//enum,not null
    "x": 10,//int
    "y": 10,//int
    "period": 1, //int
    "period_type": "match_event_period",//enum
    "outcome": "string",
    "break_name": "string"
}
```
### match_event_player
```json
{
    "player_id": "string",//not null
    "match_event_id": "string", //not null
    "type": "string"
}
```
## Enum
### team_type
- home
- away
### match_score_period_type
- regular_period
- overtime
- penalties
- entire
### match_event_period
- regular_period
- overtime
- penalties
- interrupted
- pause
### bool_type
- y
- n
### stage_type
- league
- cup
### group_standing_type
- total
- home
- away
