## sport
```
[
    {
        "id":"1",
        "name":"football"
    },
    {
        "id":"2",
        "name":"basketball"
    }
]
```
## league
```json 
[
    {
        "id": "40253", //英超
        "name": "ngland - Premier League",
        "ourId":9
    },
    {
        "id": "40031", //西甲
        "name": "Spain - La Liga",
        "ourId": 3

    },
    {
        "id": "ˋ40030", //意甲
        "name": "Italy - Serie A",
        "ourId": 15

    },
    {
        "id": "40481", //德甲
        "name": "Germany - 1.Bundesliga",
        "ourId":23

    },
    {
        "id": "40032", //法甲
        "name": "France - Ligue 1",
        "ourId":22

    },
    {
        "id": "42648",
        "name": "NBA",
    },
    {
        "id": "28455",
        "name": "CBA"
    }
]

```
## team 
### 英超
```json
[
  {
    "id": "228",
    "name_en": "Fulham",
    "name_cn": "富勒姆"
  },
  {
    "id": "231",
    "name_en": "Chelsea",
    "name_cn": "切尔西"
  },
  {
    "id": "232",
    "name_en": "Aston Villa",
    "name_cn": "阿斯顿维拉"
  },
  {
    "id": "233",
    "name_en": "Crystal Palace",
    "name_cn": "水晶宫"
  },
  {
    "id": "235",
    "name_en": "Man Utd",
    "name_cn": "曼联"
  },
  {
    "id": "238",
    "name_en": "Newcastle",
    "name_cn": "纽卡斯尔"
  },
  {
    "id": "239",
    "name_en": "Everton",
    "name_cn": "埃弗顿"
  },
  {
    "id": "240",
    "name_en": "Man City",
    "name_cn": "曼城"
  },
  {
    "id": "242",
    "name_en": "Tottenham",
    "name_cn": "托特纳姆热刺"
  },
  {
    "id": "246",
    "name_en": "Liverpool",
    "name_cn": "利物浦"
  },
  {
    "id": "247",
    "name_en": "Southampton",
    "name_cn": "南安普顿"
  },
  {
    "id": "248",
    "name_en": "Arsenal",
    "name_cn": "阿森纳"
  },
  {
    "id": "372",
    "name_en": "Brighton",
    "name_cn": "布莱顿队"
  },
  {
    "id": "375",
    "name_en": "Wolves",
    "name_cn": "狼队"
  },
  {
    "id": "376",
    "name_en": "West Ham",
    "name_cn": "西汉姆"
  },
  {
    "id": "379",
    "name_en": "Leeds",
    "name_cn": "利兹联"
  },
  {
    "id": "381",
    "name_en": "Burnley",
    "name_cn": "伯恩利"
  },
  {
    "id": "389",
    "name_en": "Leicester",
    "name_cn": "莱切斯特城"
  },
  {
    "id": "831",
    "name_en": "West Brom",
    "name_cn": "西布罗姆维奇"
  },
  {
    "id": "3251",
    "name_en": "Sheffield Utd",
    "name_cn": "谢菲联"
  },
  
]
```
### 西甲
```json
[
  {
    "id": "286",
    "name_en": "Barcelona",
    "name_cn": "巴塞罗那"
  },
  {
    "id": "287",
    "name_en": "Levante",
    "name_cn": "莱万特"
  },
  {
    "id": "293",
    "name_en": "Villarreal",
    "name_cn": "比利亚雷亚尔"
  },
  {
    "id": "298",
    "name_en": "Valencia",
    "name_cn": "瓦伦西亚"
  },
  {
    "id": "300",
    "name_en": "Getafe",
    "name_cn": "格塔菲"
  },
  {
    "id": "302",
    "name_en": "Osasuna",
    "name_cn": "奥萨苏纳"
  },
  {
    "id": "304",
    "name_en": "Real Madrid",
    "name_cn": "皇家马德里"
  },
  {
    "id": "305",
    "name_en": "Sevilla",
    "name_cn": "塞维利亚"
  },
  {
    "id": "306",
    "name_en": "Eibar",
    "name_cn": "埃瓦尔"
  },
  {
    "id": "308",
    "name_en": "Valladolid",
    "name_cn": "瓦拉多利德"
  },
  {
    "id": "315",
    "name_en": "Celta Vigo",
    "name_cn": "维戈塞尔塔"
  },
  {
    "id": "321",
    "name_en": "Alaves",
    "name_cn": "艾拉维斯"
  },
  {
    "id": "325",
    "name_en": "Elche",
    "name_cn": "艾尔切"
  },
  {
    "id": "2688",
    "name_en": "Cadiz",
    "name_cn": "卡迪斯"
  },
  {
    "id": "6018",
    "name_en": "Real Betis",
    "name_cn": "皇家贝迪斯"
  },
  {
    "id": "6485",
    "name_en": "Real Sociedad",
    "name_cn": "皇家社会"
  },
  {
    "id": "14907",
    "name_en": "Atletico Madrid",
    "name_cn": "马德里竞技"
  },
  {
    "id": "15355",
    "name_en": "Athletic Bilbao",
    "name_cn": "毕尔巴鄂竞技"
  },
  {
    "id": "40225",
    "name_en": "Granada CF",
    "name_cn": "格拉纳达"
  },
  {
    "id": "56453",
    "name_en": "SD Huesca",
    "name_cn": "韦斯卡体育协会"
  }
]
```
### 意甲
```json
[
  {
    "id": "953",
    "name_en": "AC Milan",
    "name_cn": "AC米兰"
  },
  {
    "id": "955",
    "name_en": "Atalanta",
    "name_cn": "亚特兰大"
  },
  {
    "id": "956",
    "name_en": "Fiorentina",
    "name_cn": "佛罗伦萨"
  },
  {
    "id": "958",
    "name_en": "Bologna",
    "name_cn": "博洛尼亚"
  },
  {
    "id": "959",
    "name_en": "Cagliari",
    "name_cn": "卡利亚里"
  },
  {
    "id": "966",
    "name_en": "Juventus",
    "name_cn": "尤文图斯"
  },
  {
    "id": "969",
    "name_en": "Sampdoria",
    "name_cn": "桑普多利亚"
  },
  {
    "id": "970",
    "name_en": "Udinese",
    "name_cn": "乌迪内斯"
  },
  {
    "id": "971",
    "name_en": "Lazio",
    "name_cn": "拉素"
  },
  {
    "id": "1131",
    "name_en": "Benevento",
    "name_cn": "贝内文托"
  },
  {
    "id": "1138",
    "name_en": "Napoli",
    "name_cn": "拿玻里"
  },
  {
    "id": "1146",
    "name_en": "Genoa",
    "name_cn": "热纳亚"
  },
  {
    "id": "1158",
    "name_en": "Crotone",
    "name_cn": "克罗托内"
  },
  {
    "id": "1160",
    "name_en": "Verona",
    "name_cn": "维罗纳"
  },
  {
    "id": "1161",
    "name_en": "Torino",
    "name_cn": "都灵"
  },
  {
    "id": "1172",
    "name_en": "Spezia",
    "name_cn": "士比西亚"
  },
  {
    "id": "1374",
    "name_en": "Parma",
    "name_cn": "帕尔马"
  },
  {
    "id": "1469",
    "name_en": "AS Roma",
    "name_cn": "AS罗马"
  },
  {
    "id": "16730",
    "name_en": "Sassuolo",
    "name_cn": "萨索洛"
  },
  {
    "id": "51252",
    "name_en": "Inter Milan",
    "name_cn": "国际米兰"
  }
]
```
### 德甲
```json
[
  {
    "id": "1434",
    "name_en": "Wolfsburg",
    "name_cn": "沃尔夫斯堡"
  },
  {
    "id": "1441",
    "name_en": "Dortmund",
    "name_cn": "多特蒙德"
  },
  {
    "id": "1533",
    "name_en": "Freiburg",
    "name_cn": "弗赖堡"
  },
  {
    "id": "1538",
    "name_en": "Stuttgart",
    "name_cn": "斯图加特"
  },
  {
    "id": "1540",
    "name_en": "Leverkusen",
    "name_cn": "拜耳勒沃库森"
  },
  {
    "id": "1545",
    "name_en": "Schalke",
    "name_cn": "沙尔克"
  },
  {
    "id": "1547",
    "name_en": "Bayern Munchen",
    "name_cn": "拜仁慕尼黑"
  },
  {
    "id": "1598",
    "name_en": "Hertha Berlin",
    "name_cn": "柏林赫塔"
  },
  {
    "id": "1614",
    "name_en": "Werder Bremen",
    "name_cn": "云达不莱梅"
  },
  {
    "id": "1857",
    "name_en": "Mainz",
    "name_cn": "美因茨"
  },
  {
    "id": "1860",
    "name_en": "Bielefeld",
    "name_cn": "比勒费尔德"
  },
  {
    "id": "1922",
    "name_en": "Eintracht Frankfurt",
    "name_cn": "法兰克福"
  },
  {
    "id": "2144",
    "name_en": "Monchengladbach",
    "name_cn": "门兴格拉德巴赫"
  },
  {
    "id": "2814",
    "name_en": "Union Berlin",
    "name_cn": "柏林联"
  },
  {
    "id": "2831",
    "name_en": "Hoffenheim",
    "name_cn": "霍芬海姆"
  },
  {
    "id": "2839",
    "name_en": "Augsburg",
    "name_cn": "奥格斯堡"
  },
  {
    "id": "3037",
    "name_en": "Cologne",
    "name_cn": "科隆"
  },
  {
    "id": "73228",
    "name_en": "RB Leipzig",
    "name_cn": "RB 莱比锡"
  }
]
```
### 法甲
```json
[
  {
    "id": "265",
    "name_en": "Lens",
    "name_cn": "朗斯"
  },
  {
    "id": "267",
    "name_en": "Dijon",
    "name_cn": "迪卓恩"
  },
  {
    "id": "270",
    "name_en": "Monaco",
    "name_cn": "摩纳哥"
  },
  {
    "id": "272",
    "name_en": "Montpellier",
    "name_cn": "蒙比利埃"
  },
  {
    "id": "273",
    "name_en": "Paris St-Germain",
    "name_cn": "巴黎圣日耳曼"
  },
  {
    "id": "275",
    "name_en": "Lille",
    "name_cn": "里尔"
  },
  {
    "id": "277",
    "name_en": "Nantes",
    "name_cn": "南特"
  },
  {
    "id": "437",
    "name_en": "Saint Etienne",
    "name_cn": "圣伊天"
  },
  {
    "id": "1344",
    "name_en": "Lyon",
    "name_cn": "里昂"
  },
  {
    "id": "1346",
    "name_en": "Nice",
    "name_cn": "尼斯"
  },
  {
    "id": "1347",
    "name_en": "Marseille",
    "name_cn": "马赛"
  },
  {
    "id": "1348",
    "name_en": "Angers",
    "name_cn": "昂热"
  },
  {
    "id": "1355",
    "name_en": "Metz",
    "name_cn": "梅斯"
  },
  {
    "id": "1356",
    "name_en": "Bordeaux",
    "name_cn": "波尔多"
  },
  {
    "id": "1360",
    "name_en": "Nimes",
    "name_cn": "奈梅斯"
  },
  {
    "id": "1505",
    "name_en": "Lorient",
    "name_cn": "罗连安特"
  },
  {
    "id": "1510",
    "name_en": "Reims",
    "name_cn": "兰斯"
  },
  {
    "id": "1861",
    "name_en": "Strasbourg",
    "name_cn": "斯特拉斯堡"
  },
  {
    "id": "2524",
    "name_en": "Stade Rennes",
    "name_cn": "雷恩"
  },
  {
    "id": "30310",
    "name_en": "Stade Brest",
    "name_cn": "布雷斯特"
  }
]
```
### NBA
```json
[
  {
    "id": "251",
    "name_en": "Houston Rockets",
    "name_cn": "休斯敦火箭"
  },
  {
    "id": "407",
    "name_en": "Detroit Pistons",
    "name_cn": "底特律活塞"
  },
  {
    "id": "432",
    "name_en": "Portland Trail Blazers",
    "name_cn": "波特兰开拓者"
  },
  {
    "id": "623",
    "name_en": "Indiana Pacers",
    "name_cn": "印第安纳步行者"
  },
  {
    "id": "747",
    "name_en": "Sacramento Kings",
    "name_cn": "萨克拉门托国王"
  },
  {
    "id": "823",
    "name_en": "Miami Heat",
    "name_cn": "迈阿密热火"
  },
  {
    "id": "1801",
    "name_en": "Milwaukee Bucks",
    "name_cn": "密尔沃基雄鹿"
  },
  {
    "id": "1803",
    "name_en": "Chicago Bulls",
    "name_cn": "芝加哥公牛"
  },
  {
    "id": "1804",
    "name_en": "Atlanta Hawks",
    "name_cn": "亚特兰大老鹰"
  },
  {
    "id": "1805",
    "name_en": "Brooklyn Nets",
    "name_cn": "布鲁克林篮网"
  },
  {
    "id": "1806",
    "name_en": "Philadelphia 76ers",
    "name_cn": "费城76人"
  },
  {
    "id": "1808",
    "name_en": "Phoenix Suns",
    "name_cn": "菲尼克斯太阳"
  },
  {
    "id": "2047",
    "name_en": "Dallas Mavericks",
    "name_cn": "达拉斯独行侠"
  },
  {
    "id": "2048",
    "name_en": "Denver Nuggets",
    "name_cn": "丹佛掘金"
  },
  {
    "id": "2226",
    "name_en": "Washington Wizards",
    "name_cn": "华盛顿奇才"
  },
  {
    "id": "7329",
    "name_en": "NY Knicks",
    "name_cn": "纽约尼克斯"
  },
  {
    "id": "8504",
    "name_en": "Memphis Grizzlies",
    "name_cn": "孟菲斯灰熊"
  },
  {
    "id": "19349",
    "name_en": "Toronto Raptors",
    "name_cn": "多伦多猛龙"
  },
  {
    "id": "19350",
    "name_en": "Boston Celtics",
    "name_cn": "波士顿凯尔特人"
  },
  {
    "id": "19651",
    "name_en": "LA Lakers",
    "name_cn": "洛杉矶湖人"
  },
  {
    "id": "19732",
    "name_en": "Orlando Magic",
    "name_cn": "奥兰多魔术"
  },
  {
    "id": "19735",
    "name_en": "Golden State Warriors",
    "name_cn": "金州勇士"
  },
  {
    "id": "19946",
    "name_en": "Minnesota Timberwolves",
    "name_cn": "明尼苏达森林狼"
  },
  {
    "id": "19947",
    "name_en": "Cleveland Cavaliers",
    "name_cn": "克利夫兰骑士"
  },
  {
    "id": "19948",
    "name_en": "San Antonio Spurs",
    "name_cn": "圣安东尼奥马刺"
  },
  {
    "id": "19949",
    "name_en": "Utah Jazz",
    "name_cn": "犹他爵士"
  },
  {
    "id": "20425",
    "name_en": "LA Clippers",
    "name_cn": "洛杉矶快船"
  },
  {
    "id": "59136",
    "name_en": "Oklahoma City Thunder",
    "name_cn": "俄克拉荷马城雷霆"
  },
  {
    "id": "127815",
    "name_en": "New Orleans Pelicans",
    "name_cn": "新奥尔良鹈鹕"
  },
  {
    "id": "184215",
    "name_en": "Charlotte Hornets",
    "name_cn": "夏洛特黄蜂"
  }
]
```