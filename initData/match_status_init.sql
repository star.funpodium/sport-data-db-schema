-- MySQL dump 10.13  Distrib 8.0.21, for macos10.15 (x86_64)
--
-- Host: 127.0.0.1    Database: sport_data
-- ------------------------------------------------------
-- Server version	5.7.32

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `match_status`
--

LOCK TABLES `match_status` WRITE;
/*!40000 ALTER TABLE `match_status` DISABLE KEYS */;
INSERT INTO `match_status` VALUES ('1st_extra','live','2020-10-27 02:05:34','2020-10-29 06:32:04'),('1st_half','live','2020-10-27 02:05:34','2020-10-29 06:32:04'),('1st_quarter','live','2020-10-29 06:32:04','2020-10-29 06:32:04'),('2nd_extra','live','2020-10-27 02:05:34','2020-10-29 06:32:04'),('2nd_half','live','2020-10-27 02:05:34','2020-10-29 06:32:04'),('2nd_quarter','live','2020-10-29 06:32:04','2020-10-29 06:32:04'),('3rd_quarter','live','2020-10-29 06:32:04','2020-10-29 06:32:04'),('4th_quarter','live','2020-10-29 06:32:04','2020-10-29 06:32:04'),('abandoned','cancel','2020-10-27 02:05:34','2020-10-27 02:05:34'),('aet','end','2020-10-27 02:17:20','2020-10-27 02:17:20'),('ap','end','2020-10-27 02:17:20','2020-10-27 02:17:20'),('awaiting_extra_time','live','2020-10-27 02:05:34','2020-10-27 02:05:34'),('awaiting_penalties','live','2020-10-27 02:05:34','2020-10-27 02:05:34'),('cancelled','cancel','2020-10-27 02:17:20','2020-10-27 02:17:20'),('closed','end','2020-10-27 02:17:20','2020-10-27 02:17:20'),('delayed','not_started','2020-10-27 02:17:20','2020-10-27 02:17:20'),('ended','end','2020-10-27 02:17:20','2020-10-27 02:17:20'),('extra_time','live','2020-10-27 02:17:20','2020-10-27 02:17:20'),('extra_time_halftime','live','2020-11-10 02:06:36','2020-11-10 02:06:36'),('full_time','end','2020-10-27 02:17:20','2020-10-27 02:32:06'),('halftime','live','2020-10-27 02:17:20','2020-10-27 02:17:20'),('interrupted','live','2020-10-27 02:05:34','2020-10-27 02:05:34'),('live','live','2020-10-27 02:05:34','2020-10-27 02:05:34'),('not_started','not_started','2020-10-27 02:00:00','2020-10-27 02:00:00'),('overtime','live','2020-10-27 02:05:34','2020-10-27 02:05:34'),('pause','live','2020-10-27 02:05:34','2020-10-27 02:05:34'),('penalties','live','2020-10-27 02:05:34','2020-10-27 02:05:34'),('postponed','postponed','2020-10-27 02:17:20','2020-10-27 02:17:20'),('start_delayed','not_started','2020-10-29 06:32:04','2020-10-29 06:32:04'),('started','live','2020-10-29 06:32:04','2020-10-29 06:32:04');
/*!40000 ALTER TABLE `match_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `match_status_translation`
--

LOCK TABLES `match_status_translation` WRITE;
/*!40000 ALTER TABLE `match_status_translation` DISABLE KEYS */;
INSERT INTO `match_status_translation` VALUES (1,'en','1st_extra','1st_extra','2020-11-04 01:13:33','2020-11-04 01:13:33'),(2,'en','1st_half','1st_half','2020-11-04 01:13:33','2020-11-04 01:13:33'),(3,'en','1st_quarter','1st_quarter','2020-11-04 01:13:33','2020-11-04 01:13:33'),(4,'en','2nd_extra','2nd_extra','2020-11-04 01:13:33','2020-11-04 01:13:33'),(5,'en','2nd_half','2nd_half','2020-11-04 01:13:33','2020-11-04 01:13:33'),(6,'en','2nd_quarter','2nd_quarter','2020-11-04 01:13:33','2020-11-04 01:13:33'),(7,'en','3rd_quarter','3rd_quarter','2020-11-04 01:13:33','2020-11-04 01:13:33'),(8,'en','4th_quarter','4th_quarter','2020-11-04 01:13:33','2020-11-04 01:13:33'),(9,'en','abandoned','abandoned','2020-11-04 01:13:33','2020-11-04 01:13:33'),(10,'en','aet','ended after extra time','2020-11-04 01:13:33','2020-11-04 01:29:23'),(11,'en','ap','ended after penalties','2020-11-04 01:13:33','2020-11-04 01:29:23'),(12,'en','awaiting_extra_time','awaiting_extra_time','2020-11-04 01:13:33','2020-11-04 01:13:33'),(13,'en','awaiting_penalties','awaiting_penalties','2020-11-04 01:13:33','2020-11-04 01:13:33'),(14,'en','cancelled','cancelled','2020-11-04 01:13:33','2020-11-04 01:13:33'),(15,'en','closed','closed','2020-11-04 01:13:33','2020-11-04 01:13:33'),(16,'en','delayed','delayed','2020-11-04 01:13:33','2020-11-04 01:13:33'),(17,'en','ended','ended','2020-11-04 01:13:33','2020-11-04 01:13:33'),(18,'en','extra_time','extra_time','2020-11-04 01:13:33','2020-11-04 01:13:33'),(19,'en','full_time','full_time','2020-11-04 01:13:33','2020-11-04 01:13:33'),(20,'en','halftime','halftime','2020-11-04 01:13:33','2020-11-04 01:13:33'),(21,'en','interrupted','interrupted','2020-11-04 01:13:33','2020-11-04 01:13:33'),(22,'en','live','live','2020-11-04 01:13:33','2020-11-04 01:13:33'),(23,'en','not_started','not_started','2020-11-04 01:13:33','2020-11-04 01:13:33'),(24,'en','overtime','overtime','2020-11-04 01:13:33','2020-11-04 01:13:33'),(25,'en','pause','pause','2020-11-04 01:13:33','2020-11-04 01:13:33'),(26,'en','penalties','penalties','2020-11-04 01:13:33','2020-11-04 01:13:33'),(27,'en','postponed','postponed','2020-11-04 01:13:33','2020-11-04 01:13:33'),(28,'en','start_delayed','start_delayed','2020-11-04 01:13:33','2020-11-04 01:13:33'),(29,'en','started','started','2020-11-04 01:13:33','2020-11-04 01:13:33'),(30,'zh','1st_extra','上半场加时','2020-11-04 01:13:33','2020-11-04 01:29:23'),(31,'zh','1st_half','上半场','2020-11-04 01:13:33','2020-11-04 01:29:23'),(32,'zh','1st_quarter','第一节','2020-11-04 01:13:33','2020-11-04 01:29:23'),(33,'zh','2nd_extra','下半场加时','2020-11-04 01:13:33','2020-11-04 01:29:23'),(34,'zh','2nd_half','下半场','2020-11-04 01:13:33','2020-11-04 01:29:23'),(35,'zh','2nd_quarter','第二节','2020-11-04 01:13:33','2020-11-04 01:29:23'),(36,'zh','3rd_quarter','第三节','2020-11-04 01:13:33','2020-11-04 01:29:23'),(37,'zh','4th_quarter','第四节','2020-11-04 01:13:33','2020-11-04 01:29:23'),(38,'zh','abandoned','放弃','2020-11-04 01:13:33','2020-11-04 01:29:23'),(39,'zh','aet','结束','2020-11-04 01:13:33','2020-11-04 01:29:23'),(40,'zh','ap','结束','2020-11-04 01:13:33','2020-11-04 01:29:23'),(41,'zh','awaiting_extra_time','等待裁判宣布额外时间','2020-11-04 01:13:33','2020-11-04 01:29:23'),(42,'zh','awaiting_penalties','等待处罚','2020-11-04 01:13:33','2020-11-04 01:29:23'),(43,'zh','cancelled','取消','2020-11-04 01:13:33','2020-11-04 01:29:23'),(44,'zh','closed','关闭','2020-11-04 01:13:33','2020-11-04 01:29:23'),(45,'zh','delayed','延误','2020-11-04 01:13:33','2020-11-04 01:29:23'),(46,'zh','ended','结束','2020-11-04 01:13:33','2020-11-04 01:29:23'),(47,'zh','extra_time','加时','2020-11-04 01:13:33','2020-11-04 01:29:23'),(48,'zh','full_time','结束','2020-11-04 01:13:33','2020-11-04 01:29:23'),(49,'zh','halftime','半场','2020-11-04 01:13:33','2020-11-04 01:29:23'),(50,'zh','interrupted','打断','2020-11-04 01:13:33','2020-11-04 01:29:23'),(51,'zh','live','进行中','2020-11-04 01:13:33','2020-11-04 01:29:23'),(52,'zh','not_started','未开赛','2020-11-04 01:13:33','2020-11-04 01:29:23'),(53,'zh','overtime','加时','2020-11-04 01:13:33','2020-11-04 01:29:23'),(54,'zh','pause','暂停','2020-11-04 01:13:33','2020-11-04 01:29:23'),(55,'zh','penalties','处罚进行中','2020-11-04 01:13:33','2020-11-04 01:29:23'),(56,'zh','postponed','推迟','2020-11-04 01:13:33','2020-11-04 01:29:23'),(57,'zh','start_delayed','延误开赛','2020-11-04 01:13:33','2020-11-04 01:29:23'),(58,'zh','started','开赛','2020-11-04 01:13:33','2020-11-04 01:29:23'),(59,'en','extra_time_halftime','extra_time_halftime','2020-11-10 02:08:21','2020-11-10 02:08:21'),(60,'zh','extra_time_halftime','加时半场','2020-11-10 02:08:21','2020-11-10 02:08:21');
/*!40000 ALTER TABLE `match_status_translation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `source_match_status`
--

LOCK TABLES `source_match_status` WRITE;
/*!40000 ALTER TABLE `source_match_status` DISABLE KEYS */;
INSERT INTO `source_match_status` VALUES (1,'sportradar','1st_extra','1st_extra','2020-10-29 06:58:12','2020-10-29 06:58:12'),(2,'sportradar','1st_half','1st_half','2020-10-29 06:58:12','2020-10-29 06:58:12'),(3,'sportradar','1st_quarter','1st_quarter','2020-10-29 06:58:12','2020-10-29 06:58:12'),(4,'sportradar','2nd_extra','2nd_extra','2020-10-29 06:58:12','2020-10-29 06:58:12'),(5,'sportradar','2nd_half','2nd_half','2020-10-29 06:58:12','2020-10-29 06:58:12'),(6,'sportradar','2nd_quarter','2nd_quarter','2020-10-29 06:58:12','2020-10-29 06:58:12'),(7,'sportradar','3rd_quarter','3rd_quarter','2020-10-29 06:58:12','2020-10-29 06:58:12'),(8,'sportradar','4th_quarter','4th_quarter','2020-10-29 06:58:12','2020-10-29 06:58:12'),(9,'sportradar','abandoned','abandoned','2020-10-29 06:58:12','2020-10-29 06:58:12'),(10,'sportradar','aet','aet','2020-10-29 06:58:12','2020-10-29 06:58:12'),(11,'sportradar','ap','ap','2020-10-29 06:58:12','2020-10-29 06:58:12'),(12,'sportradar','awaiting_extra_time','awaiting_extra_time','2020-10-29 06:58:12','2020-10-29 06:58:12'),(13,'sportradar','awaiting_penalties','awaiting_penalties','2020-10-29 06:58:12','2020-10-29 06:58:12'),(14,'sportradar','cancelled','cancelled','2020-10-29 06:58:12','2020-10-29 06:58:12'),(15,'sportradar','closed','closed','2020-10-29 06:58:12','2020-10-29 06:58:12'),(16,'sportradar','delayed','delayed','2020-10-29 06:58:12','2020-10-29 06:58:12'),(17,'sportradar','ended','ended','2020-10-29 06:58:12','2020-10-29 06:58:12'),(18,'sportradar','extra_time','extra_time','2020-10-29 06:58:12','2020-10-29 06:58:12'),(19,'sportradar','full-time','full_time','2020-10-29 06:58:12','2020-10-29 06:58:26'),(20,'sportradar','halftime','halftime','2020-10-29 06:58:12','2020-10-29 06:58:12'),(21,'sportradar','interrupted','interrupted','2020-10-29 06:58:12','2020-10-29 06:58:12'),(22,'sportradar','live','live','2020-10-29 06:58:12','2020-10-29 06:58:12'),(23,'sportradar','not_started','not_started','2020-10-29 06:58:12','2020-10-29 06:58:12'),(24,'sportradar','overtime','overtime','2020-10-29 06:58:12','2020-10-29 06:58:12'),(25,'sportradar','pause','pause','2020-10-29 06:58:12','2020-10-29 06:58:12'),(26,'sportradar','penalties','penalties','2020-10-29 06:58:12','2020-10-29 06:58:12'),(27,'sportradar','postponed','postponed','2020-10-29 06:58:12','2020-10-29 06:58:12'),(28,'sportradar','start_delayed','start_delayed','2020-10-29 06:58:12','2020-10-29 06:58:12'),(29,'sportradar','started','started','2020-10-29 06:58:12','2020-10-29 06:58:12'),(32,'sportradar','extra_time_halftime','extra_time_halftime','2020-11-10 02:09:25','2020-11-10 02:09:25');
/*!40000 ALTER TABLE `source_match_status` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-11-10 10:10:36
