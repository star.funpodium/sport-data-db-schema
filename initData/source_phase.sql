/*
-- Query: SELECT * FROM sport_data.source_phase
LIMIT 0, 1000

-- Date: 2021-02-18 11:52
*/
INSERT INTO `source_phase` (`id`,`vendor`,`source_id`,`phase_id`,`create_at`,`updated_at`) VALUES (1,'sportradar','1st_part_of_season_1st_leg','1st_part_of_season_1st_leg','2021-02-18 03:51:52','2021-02-18 03:51:52');
INSERT INTO `source_phase` (`id`,`vendor`,`source_id`,`phase_id`,`create_at`,`updated_at`) VALUES (2,'sportradar','2nd_part_of_season_2nd_leg','2nd_part_of_season_2nd_leg','2021-02-18 03:51:52','2021-02-18 03:51:52');
INSERT INTO `source_phase` (`id`,`vendor`,`source_id`,`phase_id`,`create_at`,`updated_at`) VALUES (3,'sportradar','3rd_round','3rd_round','2021-02-18 03:51:52','2021-02-18 03:51:52');
INSERT INTO `source_phase` (`id`,`vendor`,`source_id`,`phase_id`,`create_at`,`updated_at`) VALUES (4,'sportradar','champions_round','champions_round','2021-02-18 03:51:52','2021-02-18 03:51:52');
INSERT INTO `source_phase` (`id`,`vendor`,`source_id`,`phase_id`,`create_at`,`updated_at`) VALUES (5,'sportradar','conference','conference','2021-02-18 03:51:52','2021-02-18 03:51:52');
INSERT INTO `source_phase` (`id`,`vendor`,`source_id`,`phase_id`,`create_at`,`updated_at`) VALUES (6,'sportradar','division','division','2021-02-18 03:51:52','2021-02-18 03:51:52');
INSERT INTO `source_phase` (`id`,`vendor`,`source_id`,`phase_id`,`create_at`,`updated_at`) VALUES (7,'sportradar','final_eight','final_eight','2021-02-18 03:51:52','2021-02-18 03:51:52');
INSERT INTO `source_phase` (`id`,`vendor`,`source_id`,`phase_id`,`create_at`,`updated_at`) VALUES (8,'sportradar','final_four','final_four','2021-02-18 03:51:52','2021-02-18 03:51:52');
INSERT INTO `source_phase` (`id`,`vendor`,`source_id`,`phase_id`,`create_at`,`updated_at`) VALUES (9,'sportradar','final_phase','final_phase','2021-02-18 03:51:52','2021-02-18 03:51:52');
INSERT INTO `source_phase` (`id`,`vendor`,`source_id`,`phase_id`,`create_at`,`updated_at`) VALUES (10,'sportradar','final_round','final_round','2021-02-18 03:51:52','2021-02-18 03:51:52');
INSERT INTO `source_phase` (`id`,`vendor`,`source_id`,`phase_id`,`create_at`,`updated_at`) VALUES (11,'sportradar','final_stage','final_stage','2021-02-18 03:51:52','2021-02-18 03:51:52');
INSERT INTO `source_phase` (`id`,`vendor`,`source_id`,`phase_id`,`create_at`,`updated_at`) VALUES (12,'sportradar','grand_final','grand_final','2021-02-18 03:51:52','2021-02-18 03:51:52');
INSERT INTO `source_phase` (`id`,`vendor`,`source_id`,`phase_id`,`create_at`,`updated_at`) VALUES (13,'sportradar','grand_finals','grand_finals','2021-02-18 03:51:52','2021-02-18 03:51:52');
INSERT INTO `source_phase` (`id`,`vendor`,`source_id`,`phase_id`,`create_at`,`updated_at`) VALUES (14,'sportradar','group_phase_1','group_phase_1','2021-02-18 03:51:52','2021-02-18 03:51:52');
INSERT INTO `source_phase` (`id`,`vendor`,`source_id`,`phase_id`,`create_at`,`updated_at`) VALUES (15,'sportradar','group_phase_2','group_phase_2','2021-02-18 03:51:52','2021-02-18 03:51:52');
INSERT INTO `source_phase` (`id`,`vendor`,`source_id`,`phase_id`,`create_at`,`updated_at`) VALUES (16,'sportradar','knockout_stage','knockout_stage','2021-02-18 03:51:52','2021-02-18 03:51:52');
INSERT INTO `source_phase` (`id`,`vendor`,`source_id`,`phase_id`,`create_at`,`updated_at`) VALUES (17,'sportradar','main_round_1','main_round_1','2021-02-18 03:51:52','2021-02-18 03:51:52');
INSERT INTO `source_phase` (`id`,`vendor`,`source_id`,`phase_id`,`create_at`,`updated_at`) VALUES (18,'sportradar','main_round_2','main_round_2','2021-02-18 03:51:52','2021-02-18 03:51:52');
INSERT INTO `source_phase` (`id`,`vendor`,`source_id`,`phase_id`,`create_at`,`updated_at`) VALUES (19,'sportradar','none','none','2021-02-18 03:51:52','2021-02-18 03:51:52');
INSERT INTO `source_phase` (`id`,`vendor`,`source_id`,`phase_id`,`create_at`,`updated_at`) VALUES (20,'sportradar','placement_matches','placement_matches','2021-02-18 03:51:52','2021-02-18 03:51:52');
INSERT INTO `source_phase` (`id`,`vendor`,`source_id`,`phase_id`,`create_at`,`updated_at`) VALUES (21,'sportradar','placement_matches_13_to_16','placement_matches_13_to_16','2021-02-18 03:51:52','2021-02-18 03:51:52');
INSERT INTO `source_phase` (`id`,`vendor`,`source_id`,`phase_id`,`create_at`,`updated_at`) VALUES (22,'sportradar','placement_matches_5_to_8','placement_matches_5_to_8','2021-02-18 03:51:52','2021-02-18 03:51:52');
INSERT INTO `source_phase` (`id`,`vendor`,`source_id`,`phase_id`,`create_at`,`updated_at`) VALUES (23,'sportradar','placement_matches_9_to_12','placement_matches_9_to_12','2021-02-18 03:51:52','2021-02-18 03:51:52');
INSERT INTO `source_phase` (`id`,`vendor`,`source_id`,`phase_id`,`create_at`,`updated_at`) VALUES (24,'sportradar','placement_matches_9_to_16','placement_matches_9_to_16','2021-02-18 03:51:52','2021-02-18 03:51:52');
INSERT INTO `source_phase` (`id`,`vendor`,`source_id`,`phase_id`,`create_at`,`updated_at`) VALUES (25,'sportradar','playoffs','playoffs','2021-02-18 03:51:52','2021-02-18 03:51:52');
INSERT INTO `source_phase` (`id`,`vendor`,`source_id`,`phase_id`,`create_at`,`updated_at`) VALUES (26,'sportradar','playout','playout','2021-02-18 03:51:52','2021-02-18 03:51:52');
INSERT INTO `source_phase` (`id`,`vendor`,`source_id`,`phase_id`,`create_at`,`updated_at`) VALUES (27,'sportradar','pre-season','pre-season','2021-02-18 03:51:52','2021-02-18 03:51:52');
INSERT INTO `source_phase` (`id`,`vendor`,`source_id`,`phase_id`,`create_at`,`updated_at`) VALUES (28,'sportradar','preliminary_round','preliminary_round','2021-02-18 03:51:52','2021-02-18 03:51:52');
INSERT INTO `source_phase` (`id`,`vendor`,`source_id`,`phase_id`,`create_at`,`updated_at`) VALUES (29,'sportradar','president_cup','president_cup','2021-02-18 03:51:52','2021-02-18 03:51:52');
INSERT INTO `source_phase` (`id`,`vendor`,`source_id`,`phase_id`,`create_at`,`updated_at`) VALUES (30,'sportradar','promotion_playoffs','promotion_playoffs','2021-02-18 03:51:52','2021-02-18 03:51:52');
INSERT INTO `source_phase` (`id`,`vendor`,`source_id`,`phase_id`,`create_at`,`updated_at`) VALUES (31,'sportradar','promotion_round','promotion_round','2021-02-18 03:51:52','2021-02-18 03:51:52');
INSERT INTO `source_phase` (`id`,`vendor`,`source_id`,`phase_id`,`create_at`,`updated_at`) VALUES (32,'sportradar','qualification','qualification','2021-02-18 03:51:52','2021-02-18 03:51:52');
INSERT INTO `source_phase` (`id`,`vendor`,`source_id`,`phase_id`,`create_at`,`updated_at`) VALUES (33,'sportradar','qualification_playoffs','qualification_playoffs','2021-02-18 03:51:52','2021-02-18 03:51:52');
INSERT INTO `source_phase` (`id`,`vendor`,`source_id`,`phase_id`,`create_at`,`updated_at`) VALUES (34,'sportradar','qualification_to_allsvenskan','qualification_to_allsvenskan','2021-02-18 03:51:52','2021-02-18 03:51:52');
INSERT INTO `source_phase` (`id`,`vendor`,`source_id`,`phase_id`,`create_at`,`updated_at`) VALUES (35,'sportradar','regular season','regular season','2021-02-18 03:51:52','2021-02-18 03:51:52');
INSERT INTO `source_phase` (`id`,`vendor`,`source_id`,`phase_id`,`create_at`,`updated_at`) VALUES (36,'sportradar','relegation_playoffs','relegation_playoffs','2021-02-18 03:51:52','2021-02-18 03:51:52');
INSERT INTO `source_phase` (`id`,`vendor`,`source_id`,`phase_id`,`create_at`,`updated_at`) VALUES (37,'sportradar','relegation_promotion','relegation_promotion','2021-02-18 03:51:52','2021-02-18 03:51:52');
INSERT INTO `source_phase` (`id`,`vendor`,`source_id`,`phase_id`,`create_at`,`updated_at`) VALUES (38,'sportradar','relegation_promotion_round','relegation_promotion_round','2021-02-18 03:51:52','2021-02-18 03:51:52');
INSERT INTO `source_phase` (`id`,`vendor`,`source_id`,`phase_id`,`create_at`,`updated_at`) VALUES (39,'sportradar','relegation_round','relegation_round','2021-02-18 03:51:52','2021-02-18 03:51:52');
INSERT INTO `source_phase` (`id`,`vendor`,`source_id`,`phase_id`,`create_at`,`updated_at`) VALUES (40,'sportradar','stage_1','stage_1','2021-02-18 03:51:52','2021-02-18 03:51:52');
INSERT INTO `source_phase` (`id`,`vendor`,`source_id`,`phase_id`,`create_at`,`updated_at`) VALUES (41,'sportradar','stage_1 no_stats','stage_1 no_stats','2021-02-18 03:51:52','2021-02-18 03:51:52');
INSERT INTO `source_phase` (`id`,`vendor`,`source_id`,`phase_id`,`create_at`,`updated_at`) VALUES (42,'sportradar','stage_1_playoff','stage_1_playoff','2021-02-18 03:51:52','2021-02-18 03:51:52');
INSERT INTO `source_phase` (`id`,`vendor`,`source_id`,`phase_id`,`create_at`,`updated_at`) VALUES (43,'sportradar','stage_2','stage_2','2021-02-18 03:51:52','2021-02-18 03:51:52');
INSERT INTO `source_phase` (`id`,`vendor`,`source_id`,`phase_id`,`create_at`,`updated_at`) VALUES (44,'sportradar','stage_2 no_stats','stage_2 no_stats','2021-02-18 03:51:52','2021-02-18 03:51:52');
INSERT INTO `source_phase` (`id`,`vendor`,`source_id`,`phase_id`,`create_at`,`updated_at`) VALUES (45,'sportradar','stage_2_placement_matches','stage_2_placement_matches','2021-02-18 03:51:52','2021-02-18 03:51:52');
INSERT INTO `source_phase` (`id`,`vendor`,`source_id`,`phase_id`,`create_at`,`updated_at`) VALUES (46,'sportradar','stage_3','stage_3','2021-02-18 03:51:52','2021-02-18 03:51:52');
INSERT INTO `source_phase` (`id`,`vendor`,`source_id`,`phase_id`,`create_at`,`updated_at`) VALUES (47,'sportradar','uefa_europa_league_playoffs','uefa_europa_league_playoffs','2021-02-18 03:51:52','2021-02-18 03:51:52');
