-- MySQL dump 10.13  Distrib 8.0.21, for macos10.15 (x86_64)
--
-- Host: 127.0.0.1    Database: sport_data
-- ------------------------------------------------------
-- Server version	5.7.32

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `source_sport`
--

LOCK TABLES `source_sport` WRITE;
/*!40000 ALTER TABLE `source_sport` DISABLE KEYS */;
INSERT INTO `source_sport` VALUES (1,'sportradar','sr:sport:2','basketball','2020-10-27 08:34:38','2020-11-05 07:16:14'),(2,'sportradar','sr:sport:1','football','2020-10-27 08:34:38','2020-11-05 07:16:14');
/*!40000 ALTER TABLE `source_sport` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `source_sport_translation`
--

LOCK TABLES `source_sport_translation` WRITE;
/*!40000 ALTER TABLE `source_sport_translation` DISABLE KEYS */;
INSERT INTO `source_sport_translation` VALUES (1,'en',1,'basketball','2020-11-05 07:17:55','2020-11-05 07:17:55'),(2,'en',2,'football','2020-11-05 07:17:55','2020-11-05 07:17:55'),(3,'zh',1,'篮球','2020-11-05 07:17:55','2020-11-05 07:17:55'),(4,'zh',2,'足球','2020-11-05 07:17:55','2020-11-05 07:17:55');
/*!40000 ALTER TABLE `source_sport_translation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `sport`
--

LOCK TABLES `sport` WRITE;
/*!40000 ALTER TABLE `sport` DISABLE KEYS */;
INSERT INTO `sport` VALUES ('basketball','2020-10-27 03:00:28','2020-10-27 03:00:28'),('football','2020-10-27 03:00:28','2020-10-27 03:00:28');
/*!40000 ALTER TABLE `sport` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `sport_translation`
--

LOCK TABLES `sport_translation` WRITE;
/*!40000 ALTER TABLE `sport_translation` DISABLE KEYS */;
INSERT INTO `sport_translation` VALUES (1,'en','football','football','2020-10-27 03:02:56','2020-10-27 03:02:56'),(2,'en','basketball','basketball','2020-10-27 03:02:56','2020-10-27 03:02:56'),(3,'zh','football','足球','2020-10-27 03:02:56','2020-10-27 03:02:56'),(4,'zh','basketball','篮球','2020-10-27 03:02:56','2020-10-27 03:02:56');
/*!40000 ALTER TABLE `sport_translation` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-11-05 15:19:01
