/*
-- Query: SELECT * FROM sport_data.season_leader_type
LIMIT 0, 1000

-- Date: 2021-02-18 11:59
*/
INSERT INTO `season_leader_type` (`id`,`create_at`,`updated_at`) VALUES ('assists','2021-02-18 03:59:01','2021-02-18 03:59:01');
INSERT INTO `season_leader_type` (`id`,`create_at`,`updated_at`) VALUES ('goals','2021-02-18 03:59:01','2021-02-18 03:59:01');
INSERT INTO `season_leader_type` (`id`,`create_at`,`updated_at`) VALUES ('goals_by_head','2021-02-18 03:59:01','2021-02-18 03:59:01');
INSERT INTO `season_leader_type` (`id`,`create_at`,`updated_at`) VALUES ('goals_by_penalty','2021-02-18 03:59:01','2021-02-18 03:59:01');
INSERT INTO `season_leader_type` (`id`,`create_at`,`updated_at`) VALUES ('minutes_played','2021-02-18 03:59:01','2021-02-18 03:59:01');
INSERT INTO `season_leader_type` (`id`,`create_at`,`updated_at`) VALUES ('own_goals','2021-02-18 03:59:01','2021-02-18 03:59:01');
INSERT INTO `season_leader_type` (`id`,`create_at`,`updated_at`) VALUES ('points','2021-02-18 03:59:01','2021-02-18 03:59:01');
INSERT INTO `season_leader_type` (`id`,`create_at`,`updated_at`) VALUES ('red_cards','2021-02-18 03:59:01','2021-02-18 03:59:01');
INSERT INTO `season_leader_type` (`id`,`create_at`,`updated_at`) VALUES ('shots_off_target','2021-02-18 03:59:01','2021-02-18 03:59:01');
INSERT INTO `season_leader_type` (`id`,`create_at`,`updated_at`) VALUES ('shots_on_target','2021-02-18 03:59:01','2021-02-18 03:59:01');
INSERT INTO `season_leader_type` (`id`,`create_at`,`updated_at`) VALUES ('yellow_cards','2021-02-18 03:59:01','2021-02-18 03:59:01');
INSERT INTO `season_leader_type` (`id`,`create_at`,`updated_at`) VALUES ('yellow_red_cards','2021-02-18 03:59:01','2021-02-18 03:59:01');
