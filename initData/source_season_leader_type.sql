/*
-- Query: SELECT * FROM sport_data.source_season_leader_type
LIMIT 0, 1000

-- Date: 2021-02-18 12:03
*/
INSERT INTO `source_season_leader_type` (`id`,`vendor`,`source_id`,`season_leader_type_id`,`create_at`,`updated_at`) VALUES (1,'sportradar','assists','assists','2021-02-18 04:03:35','2021-02-18 04:03:35');
INSERT INTO `source_season_leader_type` (`id`,`vendor`,`source_id`,`season_leader_type_id`,`create_at`,`updated_at`) VALUES (2,'sportradar','goals','goals','2021-02-18 04:03:35','2021-02-18 04:03:35');
INSERT INTO `source_season_leader_type` (`id`,`vendor`,`source_id`,`season_leader_type_id`,`create_at`,`updated_at`) VALUES (3,'sportradar','goals_by_head','goals_by_head','2021-02-18 04:03:35','2021-02-18 04:03:35');
INSERT INTO `source_season_leader_type` (`id`,`vendor`,`source_id`,`season_leader_type_id`,`create_at`,`updated_at`) VALUES (4,'sportradar','goals_by_penalty','goals_by_penalty','2021-02-18 04:03:35','2021-02-18 04:03:35');
INSERT INTO `source_season_leader_type` (`id`,`vendor`,`source_id`,`season_leader_type_id`,`create_at`,`updated_at`) VALUES (5,'sportradar','minutes_played','minutes_played','2021-02-18 04:03:35','2021-02-18 04:03:35');
INSERT INTO `source_season_leader_type` (`id`,`vendor`,`source_id`,`season_leader_type_id`,`create_at`,`updated_at`) VALUES (6,'sportradar','own_goals','own_goals','2021-02-18 04:03:35','2021-02-18 04:03:35');
INSERT INTO `source_season_leader_type` (`id`,`vendor`,`source_id`,`season_leader_type_id`,`create_at`,`updated_at`) VALUES (7,'sportradar','points','points','2021-02-18 04:03:35','2021-02-18 04:03:35');
INSERT INTO `source_season_leader_type` (`id`,`vendor`,`source_id`,`season_leader_type_id`,`create_at`,`updated_at`) VALUES (8,'sportradar','red_cards','red_cards','2021-02-18 04:03:35','2021-02-18 04:03:35');
INSERT INTO `source_season_leader_type` (`id`,`vendor`,`source_id`,`season_leader_type_id`,`create_at`,`updated_at`) VALUES (9,'sportradar','shots_off_target','shots_off_target','2021-02-18 04:03:35','2021-02-18 04:03:35');
INSERT INTO `source_season_leader_type` (`id`,`vendor`,`source_id`,`season_leader_type_id`,`create_at`,`updated_at`) VALUES (10,'sportradar','shots_on_target','shots_on_target','2021-02-18 04:03:35','2021-02-18 04:03:35');
INSERT INTO `source_season_leader_type` (`id`,`vendor`,`source_id`,`season_leader_type_id`,`create_at`,`updated_at`) VALUES (11,'sportradar','yellow_cards','yellow_cards','2021-02-18 04:03:35','2021-02-18 04:03:35');
INSERT INTO `source_season_leader_type` (`id`,`vendor`,`source_id`,`season_leader_type_id`,`create_at`,`updated_at`) VALUES (12,'sportradar','yellow_red_cards','yellow_red_cards','2021-02-18 04:03:35','2021-02-18 04:03:35');
