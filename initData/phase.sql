/*
-- Query: SELECT * FROM sport_data.phase
LIMIT 0, 1000

-- Date: 2021-02-18 11:46
*/
INSERT INTO `phase` (`id`,`create_at`,`updated_at`) VALUES ('1st_part_of_season_1st_leg','2021-02-18 03:44:25','2021-02-18 03:44:25');
INSERT INTO `phase` (`id`,`create_at`,`updated_at`) VALUES ('2nd_part_of_season_2nd_leg','2021-02-18 03:44:25','2021-02-18 03:44:25');
INSERT INTO `phase` (`id`,`create_at`,`updated_at`) VALUES ('3rd_round','2021-02-18 03:44:25','2021-02-18 03:44:25');
INSERT INTO `phase` (`id`,`create_at`,`updated_at`) VALUES ('champions_round','2021-02-18 03:44:25','2021-02-18 03:44:25');
INSERT INTO `phase` (`id`,`create_at`,`updated_at`) VALUES ('conference','2021-02-18 03:44:25','2021-02-18 03:44:25');
INSERT INTO `phase` (`id`,`create_at`,`updated_at`) VALUES ('division','2021-02-18 03:44:25','2021-02-18 03:44:25');
INSERT INTO `phase` (`id`,`create_at`,`updated_at`) VALUES ('final_eight','2021-02-18 03:44:25','2021-02-18 03:44:25');
INSERT INTO `phase` (`id`,`create_at`,`updated_at`) VALUES ('final_four','2021-02-18 03:44:25','2021-02-18 03:44:25');
INSERT INTO `phase` (`id`,`create_at`,`updated_at`) VALUES ('final_phase','2021-02-18 03:44:25','2021-02-18 03:44:25');
INSERT INTO `phase` (`id`,`create_at`,`updated_at`) VALUES ('final_round','2021-02-18 03:44:25','2021-02-18 03:44:25');
INSERT INTO `phase` (`id`,`create_at`,`updated_at`) VALUES ('final_stage','2021-02-18 03:44:25','2021-02-18 03:44:25');
INSERT INTO `phase` (`id`,`create_at`,`updated_at`) VALUES ('grand_final','2021-02-18 03:44:25','2021-02-18 03:44:25');
INSERT INTO `phase` (`id`,`create_at`,`updated_at`) VALUES ('grand_finals','2021-02-18 03:44:25','2021-02-18 03:44:25');
INSERT INTO `phase` (`id`,`create_at`,`updated_at`) VALUES ('group_phase_1','2021-02-18 03:44:25','2021-02-18 03:44:25');
INSERT INTO `phase` (`id`,`create_at`,`updated_at`) VALUES ('group_phase_2','2021-02-18 03:44:25','2021-02-18 03:44:25');
INSERT INTO `phase` (`id`,`create_at`,`updated_at`) VALUES ('knockout_stage','2021-02-18 03:44:25','2021-02-18 03:44:25');
INSERT INTO `phase` (`id`,`create_at`,`updated_at`) VALUES ('main_round_1','2021-02-18 03:44:25','2021-02-18 03:44:25');
INSERT INTO `phase` (`id`,`create_at`,`updated_at`) VALUES ('main_round_2','2021-02-18 03:44:25','2021-02-18 03:44:25');
INSERT INTO `phase` (`id`,`create_at`,`updated_at`) VALUES ('none','2021-02-18 03:44:25','2021-02-18 03:44:25');
INSERT INTO `phase` (`id`,`create_at`,`updated_at`) VALUES ('placement_matches','2021-02-18 03:44:25','2021-02-18 03:44:25');
INSERT INTO `phase` (`id`,`create_at`,`updated_at`) VALUES ('placement_matches_13_to_16','2021-02-18 03:44:25','2021-02-18 03:44:25');
INSERT INTO `phase` (`id`,`create_at`,`updated_at`) VALUES ('placement_matches_5_to_8','2021-02-18 03:44:25','2021-02-18 03:44:25');
INSERT INTO `phase` (`id`,`create_at`,`updated_at`) VALUES ('placement_matches_9_to_12','2021-02-18 03:44:25','2021-02-18 03:44:25');
INSERT INTO `phase` (`id`,`create_at`,`updated_at`) VALUES ('placement_matches_9_to_16','2021-02-18 03:44:25','2021-02-18 03:44:25');
INSERT INTO `phase` (`id`,`create_at`,`updated_at`) VALUES ('playoffs','2021-02-18 03:44:25','2021-02-18 03:44:25');
INSERT INTO `phase` (`id`,`create_at`,`updated_at`) VALUES ('playout','2021-02-18 03:44:25','2021-02-18 03:44:25');
INSERT INTO `phase` (`id`,`create_at`,`updated_at`) VALUES ('pre-season','2021-02-18 03:44:25','2021-02-18 03:44:25');
INSERT INTO `phase` (`id`,`create_at`,`updated_at`) VALUES ('preliminary_round','2021-02-18 03:44:25','2021-02-18 03:44:25');
INSERT INTO `phase` (`id`,`create_at`,`updated_at`) VALUES ('president_cup','2021-02-18 03:44:25','2021-02-18 03:44:25');
INSERT INTO `phase` (`id`,`create_at`,`updated_at`) VALUES ('promotion_playoffs','2021-02-18 03:44:25','2021-02-18 03:44:25');
INSERT INTO `phase` (`id`,`create_at`,`updated_at`) VALUES ('promotion_round','2021-02-18 03:44:25','2021-02-18 03:44:25');
INSERT INTO `phase` (`id`,`create_at`,`updated_at`) VALUES ('qualification','2021-02-18 03:44:25','2021-02-18 03:44:25');
INSERT INTO `phase` (`id`,`create_at`,`updated_at`) VALUES ('qualification_playoffs','2021-02-18 03:44:25','2021-02-18 03:44:25');
INSERT INTO `phase` (`id`,`create_at`,`updated_at`) VALUES ('qualification_to_allsvenskan','2021-02-18 03:44:25','2021-02-18 03:44:25');
INSERT INTO `phase` (`id`,`create_at`,`updated_at`) VALUES ('regular season','2021-02-18 03:44:25','2021-02-18 03:44:25');
INSERT INTO `phase` (`id`,`create_at`,`updated_at`) VALUES ('relegation_playoffs','2021-02-18 03:44:25','2021-02-18 03:44:25');
INSERT INTO `phase` (`id`,`create_at`,`updated_at`) VALUES ('relegation_promotion','2021-02-18 03:44:25','2021-02-18 03:44:25');
INSERT INTO `phase` (`id`,`create_at`,`updated_at`) VALUES ('relegation_promotion_round','2021-02-18 03:44:25','2021-02-18 03:44:25');
INSERT INTO `phase` (`id`,`create_at`,`updated_at`) VALUES ('relegation_round','2021-02-18 03:44:25','2021-02-18 03:44:25');
INSERT INTO `phase` (`id`,`create_at`,`updated_at`) VALUES ('stage_1','2021-02-18 03:44:25','2021-02-18 03:44:25');
INSERT INTO `phase` (`id`,`create_at`,`updated_at`) VALUES ('stage_1 no_stats','2021-02-18 03:44:25','2021-02-18 03:44:25');
INSERT INTO `phase` (`id`,`create_at`,`updated_at`) VALUES ('stage_1_playoff','2021-02-18 03:44:25','2021-02-18 03:44:25');
INSERT INTO `phase` (`id`,`create_at`,`updated_at`) VALUES ('stage_2','2021-02-18 03:44:25','2021-02-18 03:44:25');
INSERT INTO `phase` (`id`,`create_at`,`updated_at`) VALUES ('stage_2 no_stats','2021-02-18 03:44:25','2021-02-18 03:44:25');
INSERT INTO `phase` (`id`,`create_at`,`updated_at`) VALUES ('stage_2_placement_matches','2021-02-18 03:44:25','2021-02-18 03:44:25');
INSERT INTO `phase` (`id`,`create_at`,`updated_at`) VALUES ('stage_3','2021-02-18 03:44:25','2021-02-18 03:44:25');
INSERT INTO `phase` (`id`,`create_at`,`updated_at`) VALUES ('uefa_europa_league_playoffs','2021-02-18 03:44:25','2021-02-18 03:44:25');
